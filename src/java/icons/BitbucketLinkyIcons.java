package icons;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class BitbucketLinkyIcons {

    public static final Icon Bitbucket = IconLoader.getIcon("/icons/bitbucket.png");

    public static class Actions {
        public static final Icon CopyBitbucketUrl = IconLoader.getIcon("/icons/copy_bitbucket_url.png");
        public static final Icon OpenInBitbucket = IconLoader.getIcon("/icons/open_in_bitbucket.png");
        public static final Icon CreatePullRequest = IconLoader.getIcon("/icons/create_pull_request.png");
    }

    public static class Settings {
        public static final Icon Failure = IconLoader.getIcon("/icons/failure.png");
        public static final Icon Success = IconLoader.getIcon("/icons/success.png");

        public static final Icon UnknownRepository = AllIcons.Process.Stop;
        public static final Icon LinkableRepository = AllIcons.RunConfigurations.Unknown;
        public static final Icon BitbucketGit = IconLoader.getIcon("/icons/bitbucket_git.png");
        public static final Icon BitbucketHg = IconLoader.getIcon("/icons/bitbucket_hg.png");
        public static final Icon BitbucketUnknown = IconLoader.getIcon("/icons/bitbucket.png");

        // animated GIF icon loaded differently
        // TODO animated icon instead
        @SuppressWarnings("ConstantConditions")
        public static final Icon Loading = new ImageIcon(Settings.class.getClassLoader().getResource("/icons/loading.gif"));
    }

    public static class PullRequestsToolWindow {
        public static final Icon Refresh = AllIcons.Actions.Refresh;
        public static final Icon Checkout = AllIcons.Actions.Install;
        public static final Icon OpenInBrowser = AllIcons.General.Web;
    }
}
