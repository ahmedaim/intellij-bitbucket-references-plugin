package com.atlassian.bitbucket.linky;

import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;

public interface BitbucketLinky {

    /**
     * @return base {@link URI} of the repository
     */
    URI getBaseUri();

    /**
     * Builds a URI to the commit view with the requested line in the file selected.
     * The file that appears in the commit is the ancestor of the requested file.
     *
     * @param commitReference reference to the commit: SHA, tag or branch name
     * @param file            file to build the URI for
     * @param lineNumber      line number to make the URI focus on, starts from 1
     * @return URI to the requested commit view with requested file (or its ancestor) selected
     * and with the focus on the line that historically corresponds to the requested line.
     * If the file or line doesn't exist in the requested commit, URI will point to the commit
     * itself, with no specific file selected.
     */
    @NotNull
    URI getCommitViewUri(@NotNull String commitReference, @NotNull VirtualFile file, int lineNumber);

    /**
     * @return {@link URI} to the source view of current branch/commit of the repository
     */
    @NotNull
    URI getSourceViewUri();

    /**
     * @return {@link URI} to the source view of current branch/commit of the provided file
     */
    @NotNull
    default URI getSourceViewUri(@NotNull VirtualFile file) {
        return getSourceViewUri(file, emptyList());
    }

    /**
     * @return {@link URI} to the source view selected lines of current branch/commit of the provided file
     */
    @NotNull
    URI getSourceViewUri(@NotNull VirtualFile file, @NotNull List<LinesSelection> linesSelections);

    /**
     * Builds URI for pull request for currently selected branch
     *
     * @return {@link Optional} with {@link URI} to pull request creation page if current branch name is known,
     * {@link Optional#empty()} otherwise
     */
    Optional<URI> getPullRequestUri();
}
