package com.atlassian.bitbucket.linky;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class LinesSelection implements Comparable<LinesSelection> {
    private final int firstLineNumber;
    private final int lastLineNumber;

    private LinesSelection(int firstLineNumber, int lastLineNumber) {
        this.firstLineNumber = firstLineNumber;
        this.lastLineNumber = lastLineNumber;
    }

    public static LinesSelection of(int firstLineNumber, int lastLineNumber) {
        if (firstLineNumber > lastLineNumber) {
            throw new IllegalArgumentException(String.format(
                    "Expected first line number '%d' be not greater than last line number '%d'",
                    firstLineNumber,
                    lastLineNumber));
        }
        return new LinesSelection(firstLineNumber, lastLineNumber);
    }

    public int getFirstLineNumber() {
        return firstLineNumber;
    }

    public int getLastLineNumber() {
        return lastLineNumber;
    }

    @Override
    public int compareTo(@NotNull LinesSelection other) {
        if (firstLineNumber == other.firstLineNumber) {
            return lastLineNumber - other.lastLineNumber;
        }

        return firstLineNumber - other.firstLineNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinesSelection that = (LinesSelection) o;
        return firstLineNumber == that.firstLineNumber &&
                lastLineNumber == that.lastLineNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstLineNumber, lastLineNumber);
    }
}
