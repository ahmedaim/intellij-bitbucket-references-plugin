package com.atlassian.bitbucket.linky;

public class Constants {

    /**
     * Second- and top-level domains of Bitbucket Cloud.
     * Do not use direct string comparison as there might be third-level domains (i.e. {@code staging.bitbucket.org}).
     */
    public static final String BITBUCKET_CLOUD_HOST = "bitbucket.org";
    public static final String BITBUCKET_CLOUD_DEV_HOST = "bb-inf.net";

    /**
     * Key that should be used to create {@link org.slf4j.Logger} instance.
     * It is easier to filter {@code idea.log} by just one expression
     * while troubleshooting.
     */
    public static final String LOGGER_NAME = "BitbucketReferences";

    private Constants() {
    }
}
