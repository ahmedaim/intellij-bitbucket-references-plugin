package com.atlassian.bitbucket.linky.configuration;

import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoryLinkingHelper;
import com.atlassian.bitbucket.linky.discovery.CloudBitbucketRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.discovery.RemoteUrl;
import com.atlassian.bitbucket.linky.discovery.ServerBitbucketRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.repository.RepositoriesKt;
import com.google.common.collect.ImmutableList;
import com.intellij.dvcs.repo.Repository;
import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.ListCellRendererWrapper;
import com.intellij.ui.components.JBTextField;
import okhttp3.HttpUrl;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.util.List;
import java.util.Optional;

public class LinkConfigurationForm implements Configurable {
    private final CloudBitbucketRepositoryDiscoverer cloudDiscoverer;
    private final ServerBitbucketRepositoryDiscoverer serverDiscoverer;

    private JPanel mainPanel;
    private ComboBox remoteComboBox;
    private ComboBox hostingTypeComboBox;
    private JBTextField hostingUrlTextField;
    private JBTextField repoUrlTextField;
    private JButton testInBrowserButton;
    private JTextField pathTextField;

    public LinkConfigurationForm(Repository repository) {
        Project project = repository.getProject();
        this.cloudDiscoverer = ServiceManager.getService(project, CloudBitbucketRepositoryDiscoverer.class);
        this.serverDiscoverer = ServiceManager.getService(project, ServerBitbucketRepositoryDiscoverer.class);

        String relativePath = VfsUtil.getRelativePath(repository.getRoot(), repository.getProject().getBaseDir());
        if (StringUtils.isBlank(relativePath)) {
            relativePath = "<Project Root>";
        }
        pathTextField.setText(relativePath);

        List<RemoteUrl> remoteUrls = RepositoriesKt.getRemoteUrls(repository);
        remoteComboBox.setModel(new CollectionComboBoxModel<>(remoteUrls));
        if (remoteUrls.size() <= 1) {
            remoteComboBox.setEnabled(false);
        }
    }

    @Override
    public void disposeUIResources() {
    }

    @Override
    @NotNull
    public JComponent createComponent() {
        return mainPanel;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket Repository Link Configuration";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public boolean isModified() {
        return StringUtils.isNotBlank(repoUrlTextField.getText());
    }

    @Override
    public void reset() {
        if (remoteComboBox.getItemCount() > 0) {
            remoteComboBox.setSelectedIndex(0);
        }
        if (hostingTypeComboBox.getItemCount() > 0) {
            hostingTypeComboBox.setSelectedIndex(0);
        }
        hostingUrlTextField.setText("");
        repoUrlTextField.setText("");
        testInBrowserButton.setEnabled(false);
    }

    @Override
    public void apply() throws ConfigurationException {
        if (isModified()) {
            RemoteUrl remoteUrl = (RemoteUrl) remoteComboBox.getSelectedItem();
            getHostingUrl().ifPresent(url -> getHelper().registerManuallyConfigured(remoteUrl, url.uri()));
        }
    }

    private Optional<HttpUrl> getHostingUrl() {
        return Optional.ofNullable(HttpUrl.parse(hostingUrlTextField.getText()));
    }

    private Optional<HttpUrl> getRepositoryUrl() {
        return getHostingUrl()
                .flatMap(url -> {
                            RemoteUrl remoteUrl = (RemoteUrl) remoteComboBox.getSelectedItem();
                            return Optional.ofNullable(getHelper().getRepositoryRelativeUrlPath(remoteUrl))
                                    .map(repositoryPath -> url.newBuilder()
                                            .addPathSegments(repositoryPath)
                                            .build());
                        }
                );
    }

    private BitbucketRepositoryLinkingHelper getHelper() {
        HostingType hostingType = (HostingType) hostingTypeComboBox.getSelectedItem();
        switch (hostingType) {
            case CLOUD:
                return cloudDiscoverer;
            case SERVER:
                return serverDiscoverer;
        }
        throw new IllegalStateException("Unknown hosting type " + hostingType);
    }

    private void createUIComponents() {
        remoteComboBox = new ComboBox();
        remoteComboBox.setRenderer(new ListCellRendererWrapper<RemoteUrl>() {
            @Override
            public void customize(JList list, RemoteUrl remoteUrl, int index, boolean selected, boolean hasFocus) {
                StringBuilder sb = new StringBuilder();
                sb.append(remoteUrl.getScheme()).append("://").append(remoteUrl.getHostname());
                if (remoteUrl.getPort() > 0) {
                    sb.append(":").append(remoteUrl.getPort());
                }
                sb.append(remoteUrl.getPath());
                setText(sb.toString());
            }
        });
        remoteComboBox.addActionListener(e -> inputChanged());

        hostingTypeComboBox = new ComboBox(new CollectionComboBoxModel<>(ImmutableList.of(HostingType.SERVER, HostingType.CLOUD)));
        hostingTypeComboBox.setRenderer(new ListCellRendererWrapper<HostingType>() {
            @Override
            public void customize(JList list, HostingType hostingType, int index, boolean selected, boolean hasFocus) {
                if (hostingType != null) {
                    setText(StringUtils.capitalize(hostingType.name().toLowerCase()));
                }
            }
        });
        hostingTypeComboBox.addActionListener(e -> inputChanged());

        hostingUrlTextField = new JBTextField();
        hostingUrlTextField.getEmptyText().setText("What is the URL of your Bitbucket instance?");
        hostingUrlTextField.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            protected void textChanged(DocumentEvent documentEvent) {
                inputChanged();
            }
        });

        testInBrowserButton = new JButton();
        testInBrowserButton.setEnabled(false);
        testInBrowserButton.addActionListener(e -> BrowserUtil.browse(repoUrlTextField.getText()));
    }

    private void inputChanged() {
        String repoUrl = getRepositoryUrl().map(HttpUrl::toString).orElse("");
        repoUrlTextField.setText(repoUrl);
        testInBrowserButton.setEnabled(StringUtils.isNotBlank(repoUrl));
    }

    private enum HostingType {
        CLOUD, SERVER
    }
}
