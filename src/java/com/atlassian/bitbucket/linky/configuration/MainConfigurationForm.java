package com.atlassian.bitbucket.linky.configuration;

import com.atlassian.bitbucket.linky.BitbucketRepository;
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService;
import com.atlassian.bitbucket.linky.discovery.CloudBitbucketRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.discovery.ServerBitbucketRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.repository.BitbucketRepositorySettings;
import com.atlassian.bitbucket.linky.repository.ProjectRepositoriesTraverser;
import com.atlassian.bitbucket.linky.repository.RepositoriesKt;
import com.intellij.dvcs.repo.Repository;
import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.ui.ColoredTreeCellRenderer;
import com.intellij.ui.JBColor;
import com.intellij.ui.JBSplitter;
import com.intellij.ui.SimpleTextAttributes;
import com.intellij.ui.treeStructure.SimpleTree;
import com.intellij.util.ui.UIUtil;
import com.intellij.util.ui.tree.TreeUtil;
import icons.BitbucketLinkyIcons;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.Duration;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.bitbucket.linky.configuration.UiUtil.repositoryRelativePath;

public class MainConfigurationForm implements SearchableConfigurable, Configurable.NoScroll {
    private static final String LET_ME_KNOW_URL = "https://bitbucket.org/atlassianlabs/intellij-bitbucket-references-plugin/issues?status=new&status=open";
    private static final String EMPTY = "empty";
    private static final String UNSUPPORTED = "unsupported";

    private final Project project;
    private final ProjectRepositoriesTraverser repositoriesTraverser;
    private final BitbucketRepositoriesService bitbucketRepositoriesService;
    private final CloudBitbucketRepositoryDiscoverer cloudBitbucketRepositoryDiscoverer;
    private final ServerBitbucketRepositoryDiscoverer serverBitbucketRepositoryDiscoverer;
    private final BitbucketRepositorySettings bitbucketRepositorySettings;

    private final Map<Repository, LinkConfigurationForm> linkableForms = new HashMap<>();
    private final Map<BitbucketRepository, LinkedRepositoryConfigurationForm> linkedForms = new HashMap<>();

    private Repository lastSelectedRepository;

    private JPanel mainPanel;

    private JBSplitter splitter;
    private JLabel letMeKnowLabel;

    private JPanel leftPanel;
    private SimpleTree repositoryTree;
    private DefaultTreeModel repositoryTreeModel;
    private DefaultMutableTreeNode repositoryTreeRoot;
    private DefaultMutableTreeNode unsupportedRepositoriesNode;
    private DefaultMutableTreeNode linkableRepositoriesNode;
    private DefaultMutableTreeNode linkedRepositoriesNode;

    private JPanel rightPanel;
    private CardLayout rightPaneCardLayout;
    private JPanel emptyStatePanel;
    private JPanel unsupportedRepositoryPanel;

    public MainConfigurationForm(Project project,
                                 ProjectRepositoriesTraverser repositoriesTraverser,
                                 BitbucketRepositoriesService bitbucketRepositoriesService,
                                 CloudBitbucketRepositoryDiscoverer cloudBitbucketRepositoryDiscoverer,
                                 ServerBitbucketRepositoryDiscoverer serverBitbucketRepositoryDiscoverer,
                                 BitbucketRepositorySettings bitbucketRepositorySettings) {
        this.project = project;
        this.repositoriesTraverser = repositoriesTraverser;
        this.bitbucketRepositoriesService = bitbucketRepositoriesService;
        this.cloudBitbucketRepositoryDiscoverer = cloudBitbucketRepositoryDiscoverer;
        this.serverBitbucketRepositoryDiscoverer = serverBitbucketRepositoryDiscoverer;
        this.bitbucketRepositorySettings = bitbucketRepositorySettings;

        // reloadRepositoriesTree might change tree selection, so first set right pane to empty
        switchRightPane(EMPTY);
        reloadRepositoriesTree();
    }

    @NotNull
    @Override
    public String getId() {
        return "com.atlassian.bitbucket.linky";
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket Linky";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public Runnable enableSearch(String option) {
        return null;
    }

    @Override
    public void disposeUIResources() {
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        return mainPanel;
    }

    @Override
    public void apply() throws ConfigurationException {
        boolean atLeastOneLinkableFormModified = false;
        for (LinkConfigurationForm form : linkableForms.values()) {
            if (form.isModified()) {
                atLeastOneLinkableFormModified = true;
            }
            form.apply();
        }
        for (LinkedRepositoryConfigurationForm form : linkedForms.values()) {
            form.apply();
        }

        if (atLeastOneLinkableFormModified) {
            SwingUtilities.invokeLater(() -> {
                repositoryTree.setEnabled(false);
                bitbucketRepositoriesService.configurationChanged(Duration.ofSeconds(10));
                reloadRepositoriesTree();
                repositoryTree.setEnabled(true);
            });
        }
    }

    @Override
    public void reset() {
        for (LinkConfigurationForm form : linkableForms.values()) {
            form.reset();
        }
        for (LinkedRepositoryConfigurationForm form : linkedForms.values()) {
            form.reset();
        }
    }

    @Override
    public boolean isModified() {
        return Stream.of(linkableForms, linkedForms)
                .flatMap(m -> m.values().stream())
                .anyMatch(Configurable::isModified);
    }

    private void createUIComponents() {
        leftPanel = new JPanel(new BorderLayout());

        rightPaneCardLayout = new CardLayout();
        rightPanel = new JPanel(rightPaneCardLayout);

        splitter = new JBSplitter("com.atlassian.bitbucket.linky.configuration", 0.35f);
        splitter.setFirstComponent(leftPanel);
        splitter.setSecondComponent(rightPanel);

        unsupportedRepositoriesNode = new DefaultMutableTreeNode("Unsupported");
        linkableRepositoriesNode = new DefaultMutableTreeNode("Not linked");
        linkedRepositoriesNode = new DefaultMutableTreeNode("Linked");

        repositoryTreeRoot = new DefaultMutableTreeNode("Root");
        repositoryTreeModel = new DefaultTreeModel(repositoryTreeRoot);

        repositoryTree = new SimpleTree(repositoryTreeModel);
        repositoryTree.setRootVisible(false);
        repositoryTree.getEmptyText().setText("No VCS repositories");
        repositoryTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        repositoryTree.setExpandsSelectedPaths(true);
        repositoryTree.setExpandableItemsEnabled(false);
        repositoryTree.setCellRenderer(new RepositoryTreeCellRenderer());
        repositoryTree.addTreeSelectionListener(new RepositoryTreeSelectionListener());
        UIUtil.setLineStyleAngled(repositoryTree);
        TreeUtil.installActions(repositoryTree);
        JScrollPane treeScrollPane = new JScrollPane(repositoryTree);
        treeScrollPane.setMinimumSize(new Dimension(150, 150));
        leftPanel.add(treeScrollPane);

        emptyStatePanel = new JPanel(new GridBagLayout());
        JLabel selectRepoLabel = new JLabel("Select a repository to configure");
        selectRepoLabel.setForeground(JBColor.GRAY);
        emptyStatePanel.add(selectRepoLabel);
        // added in reloadRepositoriesTree()

        unsupportedRepositoryPanel = new JPanel(new GridBagLayout());
        JLabel unsupportedRepositoryLabel = new JLabel("Selected repository doesn't have remote URLs that match Bitbucket pattern");
        unsupportedRepositoryLabel.setForeground(JBColor.GRAY);
        unsupportedRepositoryPanel.add(unsupportedRepositoryLabel);
        // added in reloadRepositoriesTree()

        letMeKnowLabel = new JLabel();
        letMeKnowLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        letMeKnowLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                BrowserUtil.browse(LET_ME_KNOW_URL);
            }
        });
    }

    private Optional<DefaultMutableTreeNode> findRepositoryNode(Repository repository) {
        if (repository != null) {
            Enumeration nodes = repositoryTreeRoot.preorderEnumeration();
            while (nodes.hasMoreElements()) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) nodes.nextElement();
                Object userObject = node.getUserObject();
                Repository nodeRepo = null;
                if (userObject instanceof Repository) {
                    nodeRepo = (Repository) userObject;
                } else if (userObject instanceof BitbucketRepository) {
                    nodeRepo = ((BitbucketRepository) userObject).getRepository();
                }
                if (repository.equals(nodeRepo)) {
                    return Optional.of(node);
                }
            }
        }
        return Optional.empty();
    }

    private void reloadRepositoriesTree() {
        List<BitbucketRepository> bitbucketRepositories = bitbucketRepositoriesService.listBitbucketRepositories();
        List<Repository> linkedRepositories = bitbucketRepositories.stream()
                .map(BitbucketRepository::getRepository)
                .collect(Collectors.toList());

        Collection<Repository> allRepositories = repositoriesTraverser.traverseRepositories();
        List<Repository> linkableRepositories = allRepositories.stream()
                .filter(repo -> !linkedRepositories.contains(repo))
                .filter(repo -> RepositoriesKt.getRemoteUrls(repo).stream()
                        .anyMatch(url -> cloudBitbucketRepositoryDiscoverer.remoteUrlMatchesPattern(url) ||
                                serverBitbucketRepositoryDiscoverer.remoteUrlMatchesPattern(url)))
                .collect(Collectors.toList());

        List<Repository> unsupportedRepositories = allRepositories.stream()
                .filter(repo -> !linkedRepositories.contains(repo))
                .filter(repo -> !linkableRepositories.contains(repo))
                .collect(Collectors.toList());

        repositoryTreeRoot.removeAllChildren();
        unsupportedRepositoriesNode.removeAllChildren();
        linkableRepositoriesNode.removeAllChildren();
        linkedRepositoriesNode.removeAllChildren();

        linkableForms.clear();
        linkedForms.clear();

        rightPanel.removeAll();
        rightPanel.add(emptyStatePanel, EMPTY);
        rightPanel.add(unsupportedRepositoryPanel, UNSUPPORTED);

        if (!unsupportedRepositories.isEmpty()) {
            repositoryTreeRoot.add(unsupportedRepositoriesNode);
            unsupportedRepositories.forEach(repo -> unsupportedRepositoriesNode.add(new DefaultMutableTreeNode(repo)));
        }
        if (!linkableRepositories.isEmpty()) {
            repositoryTreeRoot.add(linkableRepositoriesNode);
            linkableRepositories.forEach(repo -> {
                linkableRepositoriesNode.add(new DefaultMutableTreeNode(repo));
                LinkConfigurationForm form = new LinkConfigurationForm(repo);
                rightPanel.add(form.createComponent(), repositoryRelativePath(repo));
                linkableForms.put(repo, form);
            });
        }
        if (!bitbucketRepositories.isEmpty()) {
            repositoryTreeRoot.add(linkedRepositoriesNode);
            bitbucketRepositories.forEach(bitbucketRepo -> {
                linkedRepositoriesNode.add(new DefaultMutableTreeNode(bitbucketRepo));
                LinkedRepositoryConfigurationForm form = new LinkedRepositoryConfigurationForm(bitbucketRepositorySettings, bitbucketRepo);
                rightPanel.add(form.createComponent(), repositoryRelativePath(bitbucketRepo.getRepository()));
                linkedForms.put(bitbucketRepo, form);
            });
        }

        repositoryTreeModel.reload(repositoryTreeRoot);

        for (int i = 0; i < repositoryTree.getRowCount(); i++) {
            repositoryTree.expandRow(i);
        }

        // preselect repository if there's only one
        if (allRepositories.size() == 1) {
            TreeNode onlyNode = ((DefaultMutableTreeNode) repositoryTreeRoot.getFirstChild()).getFirstChild();
            selectTreeNode(onlyNode);
        } else {
            findRepositoryNode(lastSelectedRepository).ifPresent(this::selectTreeNode);
        }
    }

    private void selectTreeNode(TreeNode node) {
        TreeNode[] pathToRoot = repositoryTreeModel.getPathToRoot(node);
        if (pathToRoot != null) {
            repositoryTree.setSelectionPath(new TreePath(pathToRoot));
        }
    }

    private void switchRightPane(String cardName) {
        rightPaneCardLayout.show(rightPanel, cardName);
    }

    private void switchRightPane(Repository repository) {
        switchRightPane(repositoryRelativePath(repository));
    }

    private class RepositoryTreeCellRenderer extends ColoredTreeCellRenderer {
        @Override
        public void customizeCellRenderer(@NotNull JTree tree,
                                          Object value,
                                          boolean selected,
                                          boolean expanded,
                                          boolean leaf,
                                          int row,
                                          boolean hasFocus) {
            if (value instanceof DefaultMutableTreeNode) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
                DefaultMutableTreeNode parent = (DefaultMutableTreeNode) node.getParent();
                if (unsupportedRepositoriesNode.equals(parent)) {
                    renderRepository((Repository) node.getUserObject());
                    setIcon(BitbucketLinkyIcons.Settings.UnknownRepository);
                } else if (linkableRepositoriesNode.equals(parent)) {
                    renderRepository((Repository) node.getUserObject());
                    setIcon(BitbucketLinkyIcons.Settings.LinkableRepository);
                } else if (linkedRepositoriesNode.equals(parent)) {
                    BitbucketRepository bitbucketRepository = (BitbucketRepository) node.getUserObject();
                    renderRepository(bitbucketRepository.getRepository());
                    if (bitbucketRepository.isGit()) {
                        setIcon(BitbucketLinkyIcons.Settings.BitbucketGit);
                    } else if (bitbucketRepository.isHg()) {
                        setIcon(BitbucketLinkyIcons.Settings.BitbucketHg);
                    } else {
                        setIcon(BitbucketLinkyIcons.Settings.BitbucketUnknown);
                    }
                } else if (repositoryTreeRoot.equals(parent)) {
                    append((String) node.getUserObject(), SimpleTextAttributes.REGULAR_BOLD_ATTRIBUTES);
                }
            }
        }

        private void renderRepository(Repository repository) {
            String relativePath = VfsUtil.getRelativePath(repository.getRoot(), project.getBaseDir());
            if (StringUtils.isBlank(relativePath)) {
                relativePath = "<Project Root>";
            }
            append(relativePath);
        }
    }

    private class RepositoryTreeSelectionListener implements TreeSelectionListener {
        @Override
        public void valueChanged(TreeSelectionEvent e) {
            TreePath selectionPath = repositoryTree.getSelectionPath();
            if (selectionPath != null) {
                DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
                Object userObject = selectedNode.getUserObject();
                if (userObject instanceof Repository) {
                    lastSelectedRepository = (Repository) userObject;
                    DefaultMutableTreeNode parent = (DefaultMutableTreeNode) selectedNode.getParent();
                    if (unsupportedRepositoriesNode.equals(parent)) {
                        switchRightPane(UNSUPPORTED);
                    } else if (linkableRepositoriesNode.equals(parent)) {
                        Repository repo = (Repository) userObject;
                        switchRightPane(repo);
                    } else {
                        throw new IllegalStateException("Unknown root node " + parent);
                    }
                } else if (userObject instanceof BitbucketRepository) {
                    BitbucketRepository bitbucketRepo = (BitbucketRepository) userObject;
                    lastSelectedRepository = bitbucketRepo.getRepository();
                    switchRightPane(bitbucketRepo.getRepository());
                } else {
                    lastSelectedRepository = null;
                    switchRightPane(EMPTY);
                }
            } else {
                switchRightPane(EMPTY);
            }
        }
    }
}
