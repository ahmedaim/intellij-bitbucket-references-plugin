package com.atlassian.bitbucket.linky.configuration;

import com.atlassian.bitbucket.linky.BitbucketRepository;
import com.atlassian.bitbucket.linky.repository.BitbucketRepositorySettings;
import com.atlassian.bitbucket.linky.repository.SettingKey;
import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.components.JBTextField;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LinkedRepositoryConfigurationForm implements Configurable {
    private final TwoStepBitbucketRepositorySettings settings;
    private final BitbucketRepository repository;

    private JBTextField branchNameTextField;
    private JPanel mainPanel;
    private JLabel urlLabel;

    public LinkedRepositoryConfigurationForm(BitbucketRepositorySettings settings, BitbucketRepository bitbucketRepository) {
        this.settings = new TwoStepBitbucketRepositorySettings(settings);
        this.repository = bitbucketRepository;

        urlLabel.setText("<html><a href=\"#\">" + bitbucketRepository.getBaseUri().toString() + "</a></html>");
    }

    @Override
    public void disposeUIResources() {
    }

    @NotNull
    @Override
    public JComponent createComponent() {
        return mainPanel;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket Repository Configuration";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public boolean isModified() {
        return settings.isModified();
    }

    @Override
    public void apply() throws ConfigurationException {
        settings.apply();
    }

    @Override
    public void reset() {
        settings.reset();
        branchNameTextField.setText(settings.getProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME, ""));
    }

    private void createUIComponents() {
        urlLabel = new JLabel();
        urlLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        urlLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                BrowserUtil.browse(repository.getBaseUri().uri());
            }
        });

        branchNameTextField = new JBTextField();
        branchNameTextField.getEmptyText().setText("<default branch>");
        branchNameTextField.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            protected void textChanged(DocumentEvent documentEvent) {
                String userInput = branchNameTextField.getText();
                String oldBranchName = settings.getProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME, "");
                if (!StringUtils.equals(userInput, oldBranchName)) {
                    if (StringUtils.isBlank(userInput)) {
                        settings.removeProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME);
                    } else {
                        settings.setProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME, userInput);
                    }
                }
            }
        });
    }
}
