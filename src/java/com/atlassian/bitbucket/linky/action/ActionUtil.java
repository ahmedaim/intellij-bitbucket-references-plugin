package com.atlassian.bitbucket.linky.action;

import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.ui.awt.RelativePoint;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.util.Optional;

public class ActionUtil {
    private ActionUtil() {
    }

    public static void openUriInBrowser(@Nullable Project project, @NotNull URI uri) {
        BrowserUtil.browse(uri.toASCIIString());
        showStatusBarPopupMessage(project, MessageType.INFO, uri.toString() + " is now being opened in browser");
    }

    public static void showStatusBarPopupMessage(@Nullable Project project,
                                                 @NotNull MessageType messageType,
                                                 @NotNull String message) {
        Optional.ofNullable(WindowManager.getInstance().getStatusBar(project))
                .ifPresent(statusBar ->
                        JBPopupFactory.getInstance()
                                .createHtmlTextBalloonBuilder(message, messageType, null)
                                .setFadeoutTime(5000)
                                .createBalloon()
                                .show(RelativePoint.getCenterOf(statusBar.getComponent()),
                                        Balloon.Position.atRight));
    }

}
