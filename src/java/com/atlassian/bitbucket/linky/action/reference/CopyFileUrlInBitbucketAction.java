package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.action.ActionUtil;
import com.intellij.openapi.ide.CopyPasteManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.datatransfer.StringSelection;
import java.net.URI;

public class CopyFileUrlInBitbucketAction extends AbstractFileUrlInBitbucketAction {
    @Override
    protected void actionPerformed(@Nullable Project project, @NotNull URI fileUri) {
        String toCopy = fileUri.toASCIIString();
        CopyPasteManager.getInstance().setContents(new StringSelection(toCopy));
        ActionUtil.showStatusBarPopupMessage(project, MessageType.INFO, toCopy + " has been copied");
    }
}
