package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.atlassian.bitbucket.linky.action.ActionUtil;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.vcs.AbstractVcs;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Optional;

public class BitbucketCreatePullRequestAction extends AbstractBitbucketReferenceAction {

    @Override
    protected void actionPerformed(@NotNull AnActionEvent event,
                                   @NotNull Project project,
                                   @NotNull VirtualFile file,
                                   @NotNull BitbucketLinky linky,
                                   @NotNull AbstractVcs vcs) {
        Optional<URI> pullRequestUri = linky.getPullRequestUri();
        if (pullRequestUri.isPresent()) {
            ActionUtil.openUriInBrowser(project, pullRequestUri.get());
        } else {
            ActionUtil.showStatusBarPopupMessage(project, MessageType.WARNING, "You should be on a branch to create a Pull Request");
        }
    }
}
