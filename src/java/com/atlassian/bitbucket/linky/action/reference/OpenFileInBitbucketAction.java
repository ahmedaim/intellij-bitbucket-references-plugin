package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.action.ActionUtil;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;

public class OpenFileInBitbucketAction extends AbstractFileUrlInBitbucketAction {
    @Override
    protected void actionPerformed(@Nullable Project project, @NotNull URI fileUri) {
        ActionUtil.openUriInBrowser(project, fileUri);
    }
}
