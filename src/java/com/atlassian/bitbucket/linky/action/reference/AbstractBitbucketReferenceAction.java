package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.atlassian.bitbucket.linky.BitbucketLinkyProvider;
import com.atlassian.bitbucket.linky.action.ActionUtil;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.vcs.AbstractVcs;
import com.intellij.openapi.vcs.FileStatus;
import com.intellij.openapi.vcs.FileStatusManager;
import com.intellij.openapi.vcs.ProjectLevelVcsManager;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Optional;
import java.util.function.Supplier;

import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;

public abstract class AbstractBitbucketReferenceAction extends DumbAwareAction {

    private static final Logger log = Logger.getInstance(LOGGER_NAME);

    public AbstractBitbucketReferenceAction() {
        this(null, null, null);
    }

    public AbstractBitbucketReferenceAction(@Nullable String text, @Nullable String description, @Nullable Icon icon) {
        super(text, description, icon);
    }

    @Override
    public final void actionPerformed(AnActionEvent e) {
        DataContext dataContext = e.getDataContext();

        Boolean performed = ofNullable(e.getProject())
                .map(project -> ofNullable(CommonDataKeys.VIRTUAL_FILE.getData(dataContext))
                        .map(file -> ofNullable(ProjectLevelVcsManager.getInstance(project).getVcsFor(file))
                                .map(vcs -> getLinky(project, file)
                                        .map(linky -> {
                                            actionPerformed(e, project, file, linky, vcs);
                                            return true;
                                        }).orElseGet(loggingSupplier("Linky not available"))
                                ).orElseGet(loggingSupplier("VCS not detected for selected file"))
                        ).orElseGet(loggingSupplier("Virtual file not defined"))
                ).orElseGet(loggingSupplier("Project not defined"));

        if (!performed) {
            ActionUtil.showStatusBarPopupMessage(e.getProject(), MessageType.WARNING,
                    format("Action '%s' could not be performed. Please check IDE logs.", e.getPresentation().getText()));
        }
    }

    @Override
    public final void update(AnActionEvent event) {
        Project project = event.getProject();
        VirtualFile file = CommonDataKeys.VIRTUAL_FILE.getData(event.getDataContext());

        if (project != null &&
                file != null &&
                isFileWithProperStatus(project, file)) {

            boolean enabled = getLinky(project, file)
                    .map(linky -> couldActionBeEnabled(project, file))
                    .orElse(false);

            log.debug(format("%s %s action for file '%s'", enabled ? "Enabled" : "Disabled", getClass().getSimpleName(), file));
            event.getPresentation().setEnabledAndVisible(enabled);
        } else {
            event.getPresentation().setEnabledAndVisible(false);
        }
    }

    protected abstract void actionPerformed(@NotNull AnActionEvent event,
                                            @NotNull Project project,
                                            @NotNull VirtualFile file,
                                            @NotNull BitbucketLinky linky,
                                            @NotNull AbstractVcs vcs);

    protected boolean couldActionBeEnabled(@NotNull Project project, @NotNull VirtualFile file) { // TODO parameter list
        return true;
    }

    private Optional<BitbucketLinky> getLinky(@NotNull Project project, @NotNull VirtualFile file) {
        return ofNullable(ServiceManager.getService(project, BitbucketLinkyProvider.class))
                .flatMap(service -> service.getBitbucketLinky(file));
    }

    private boolean isFileWithProperStatus(@NotNull Project project, @NotNull VirtualFile file) {
        FileStatusManager fileStatusManager = FileStatusManager.getInstance(project);
        FileStatus status = fileStatusManager.getStatus(file);
        return status != FileStatus.UNKNOWN && status != FileStatus.IGNORED;
    }

    @NotNull
    private Supplier<Boolean> loggingSupplier(String message) {
        return () -> {
            log.warn(message);
            return false;
        };
    }

}
