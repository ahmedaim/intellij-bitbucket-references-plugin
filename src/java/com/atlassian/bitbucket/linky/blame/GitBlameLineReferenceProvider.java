package com.atlassian.bitbucket.linky.blame;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Pair;
import com.intellij.openapi.vcs.VcsException;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.vcsUtil.VcsUtil;
import git4idea.GitUtil;
import git4idea.commands.GitCommand;
import git4idea.commands.GitSimpleHandler;
import git4idea.repo.GitRepository;
import git4idea.repo.GitRepositoryManager;
import git4idea.util.StringScanner;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;
import static com.atlassian.bitbucket.linky.util.Optionals.parseInteger;
import static java.lang.String.format;
import static java.util.Optional.*;

public class GitBlameLineReferenceProvider implements BlameLineReferenceProvider {
    protected static final Logger log = Logger.getInstance(LOGGER_NAME);

    private static final String KEY_FILENAME = "filename";

    private final Project project;

    public GitBlameLineReferenceProvider(Project project) {
        this.project = project;
    }

    @NotNull
    @Override
    public Optional<Pair<String, Integer>> getBlameLineReference(@NotNull VirtualFile file, int lineNumber) {
        GitRepositoryManager repositoryManager = GitUtil.getRepositoryManager(project);

        return ofNullable(repositoryManager.getRepositoryForFile(file))
                .map(repository -> createBlameCommand(repository, file, lineNumber))
                .flatMap(this::runCommand)
                .flatMap(this::parseReferenceFromBlameOutput);
    }

    private GitSimpleHandler createBlameCommand(GitRepository repository, VirtualFile file, int lineNumber) {
        GitSimpleHandler handler = new GitSimpleHandler(project, repository.getRoot(), GitCommand.BLAME);
        handler.setStdoutSuppressed(true);
        handler.setCharset(file.getCharset());
        // git blame -p -L51,+1 -M -n -w -- path/to/the/file.txt
        handler.addParameters("--porcelain", format("-L%d,+1", lineNumber), "-M", "-n", "-w", "HEAD");
        handler.endOptions();
        handler.addRelativePaths(VcsUtil.getFilePath(file));
        return handler;
    }

    private Optional<String> runCommand(GitSimpleHandler handler) {
        try {
            log.debug(format("Running command '%s'", handler));
            return of(handler.run());
        } catch (VcsException e) {
            log.error(format("Failed to run command '%s'", handler), e);
            return empty();
        }
    }

    private Optional<Pair<String, Integer>> parseReferenceFromBlameOutput(String output) {
        StringScanner s = new StringScanner(output);
        s.spaceToken(); // skip commit hash
        String strLineNumber = s.spaceToken();
        s.nextLine();

        Optional<String> maybeFilePath = empty();
        while (s.hasMoreData() && !s.startsWith('\t')) {
            String key = s.spaceToken();
            String value = s.line();
            if (KEY_FILENAME.equals(key)) {
                maybeFilePath = of(value);
                break;
            }
        }

        return maybeFilePath
                .flatMap(filePath ->
                        parseInteger(strLineNumber, e ->
                                log.error(format("Failed to parse line number from git blame output '%s'", output)))
                                .map(lineNumber -> Pair.create(filePath, lineNumber)));
    }
}
