package com.atlassian.bitbucket.linky.blame;

import com.intellij.openapi.util.Pair;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public interface BlameLineReferenceProvider {

    /**
     * Calculates a path to the file and a line number that together correspond to the requested line
     * of current version of the file.
     * Practically, does {@code blame} for a specific line and gets resulting file (might differ from
     * the requested if that file has been renamed later than the line was changed last time)
     * and number of the line that corresponds to the requested one.
     */
    @NotNull
    Optional<Pair<String, Integer>> getBlameLineReference(@NotNull VirtualFile file, int lineNumber);
}
