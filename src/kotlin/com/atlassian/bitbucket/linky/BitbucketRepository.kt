package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.hosting.BitbucketCloud
import com.atlassian.bitbucket.linky.hosting.BitbucketServer
import com.atlassian.bitbucket.linky.repository.isGit
import com.atlassian.bitbucket.linky.repository.isHg
import com.intellij.dvcs.repo.Repository
import okhttp3.HttpUrl

sealed class BitbucketRepository(val repository: Repository, val slug: String) {
    abstract val baseUri: HttpUrl

    class Cloud(repository: Repository,
                val hosting: BitbucketCloud,
                val owner: String,
                slug: String) : BitbucketRepository(repository, slug) {
        override val baseUri: HttpUrl
            get() = HttpUrl.get(hosting.baseUri).newBuilder()
                    .addPathSegment(owner)
                    .addPathSegment(slug)
                    .build()

        override fun toString(): String {
            return "BitbucketCloudRepository{$owner/$slug}"
        }
    }

    class Server(repository: Repository,
                 val hosting: BitbucketServer,
                 val projectKey: String,
                 slug: String) : BitbucketRepository(repository, slug) {
        override val baseUri: HttpUrl
            get() = HttpUrl.get(hosting.baseUri).newBuilder()
                    .addPathSegment("projects").addPathSegment(projectKey)
                    .addPathSegment("repos")
                    .addPathSegment(slug)
                    .build()

        override fun toString(): String {
            return "BitbucketServerRepository{$projectKey/$slug}"
        }
    }

    fun isGit() = repository.isGit()
    fun isHg() = repository.isHg()
}
