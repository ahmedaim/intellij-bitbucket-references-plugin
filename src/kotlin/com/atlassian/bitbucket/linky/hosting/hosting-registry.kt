package com.atlassian.bitbucket.linky.hosting

import com.atlassian.bitbucket.linky.Constants.LOGGER_NAME
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.HTTP
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.diagnostic.Logger
import org.jdom.Element
import java.net.URI
import java.util.*
import java.util.function.Function

private val log = Logger.getInstance(LOGGER_NAME)

open class AbstractHostingRegistry<T : Hosting> : HostingRegistry<T> {
    private val hostings: MutableMap<UUID, T> = mutableMapOf()

    override fun get(id: UUID) = hostings.get(id)

    override fun get(baseUri: URI) = hostings.values.find { it.baseUri == baseUri }

    override fun list() = hostings.values.toList()

    override fun remove(hosting: T) {
        hostings.remove(hosting.id)
    }

    protected fun getOrAdd(baseUri: URI, create: (UUID) -> T): T =
            get(baseUri) ?: create.invoke(generateUuid()).let {
                hostings.put(it.id, it)
                it
            }

    protected open fun loadState(element: Element, createInstance: (Element) -> T?) {
        hostings.clear()
        element.getChild("instances").getChildren("instance")
                .map { createInstance.invoke(it) }
                .filterNotNull()
                .forEach { hostings.put(it.id, it) }
    }

    protected open fun getState(): Element {
        return Element("instances").apply {
            hostings.toSortedMap().forEach { e ->
                addContent(Element("instance").apply {
                    setAttribute("uuid", e.key.toString())
                    setAttribute("baseUri", e.value.baseUri.toString())
                })
            }
        }
    }

    private tailrec fun generateUuid(): UUID {
        val uuid = UUID.randomUUID()
        if (get(uuid) == null) {
            return uuid
        } else {
            return generateUuid()
        }
    }
}

@State(name = "BitbucketCloudRegistry", storages = arrayOf(Storage("bitbucket-cloud-registry.xml"), Storage(value = "bitbucket-hosting-registry.xml", deprecated = true)))
class DefaultBitbucketCloudRegistry : AbstractHostingRegistry<BitbucketCloud>(), BitbucketCloudRegistry, PersistentStateComponent<Element> {
    private val lookupMap: MutableMap<BitbucketCloudCoordinates, UUID> = mutableMapOf()

    override fun getOrAdd(scheme: UriScheme, hostname: String, port: Int, uri: URI): BitbucketCloud {
        lookup(scheme, hostname, port).let {
            if (it == null) {
                val cloud = getOrAdd(uri) { uuid -> BitbucketCloud(uuid, uri) }
                lookupMap.put(BitbucketCloudCoordinates(scheme, hostname, port), cloud.id)
                return cloud
            } else {
                return it
            }
        }
    }

    override fun lookup(scheme: UriScheme, hostname: String, port: Int): BitbucketCloud? {
        return lookupMap[BitbucketCloudCoordinates(scheme, hostname, port)]?.let { get(it) }
    }

    override fun getState(): Element {
        return Element("BitbucketCloudRegistry").apply {
            addContent(super.getState())
            addContent(Element("lookup").apply {
                lookupMap.toSortedMap().forEach { e ->
                    addContent(Element("record").apply {
                        setAttribute("scheme", e.key.scheme.toString())
                        setAttribute("hostname", e.key.hostname)
                        setAttribute("port", e.key.port.toString())
                        setAttribute("uuid", e.value.toString())
                    })
                }
            })
        }
    }

    override fun loadState(element: Element) {
        super.loadState(element, {
            val uuid = it.getUuid("uuid")
            val baseUri = it.getUri("baseUri")
            if (uuid != null && baseUri != null) BitbucketCloud(uuid, baseUri) else null
        })

        lookupMap.clear()
        element.getChild("lookup").getChildren("record")
                .forEach {
                    val uriScheme = it.getUriScheme("scheme")
                    val hostname = it.getString("hostname")
                    val port = it.getInt("port")
                    val uuid = it.getUuid("uuid")
                    if (uriScheme != null && hostname != null && port != null && uuid != null) {
                        lookupMap.put(BitbucketCloudCoordinates(uriScheme, hostname, port), uuid)
                    }
                }
    }

    data class BitbucketCloudCoordinates(val scheme: UriScheme = HTTP, val hostname: String, val port: Int) : Comparable<BitbucketCloudCoordinates> {
        override fun compareTo(other: BitbucketCloudCoordinates) =
                Comparator.comparing(Function<BitbucketCloudCoordinates, String> { t -> t.hostname })
                        .thenComparing { t -> t.port }
                        .thenComparing { t -> t.scheme }
                        .compare(this, other)
    }
}

@State(name = "BitbucketServerRegistry", storages = arrayOf(Storage("bitbucket-server-registry.xml"), Storage(value = "bitbucket-hosting-registry.xml", deprecated = true)))
class DefaultBitbucketServerRegistry : AbstractHostingRegistry<BitbucketServer>(), BitbucketServerRegistry, PersistentStateComponent<Element> {
    private val lookupMap: MutableMap<BitbucketServerCoordinates, UUID> = mutableMapOf()

    override fun getOrAdd(scheme: UriScheme, hostname: String, port: Int, path: String, uri: URI): BitbucketServer {
        lookup(scheme, hostname, port, path).let {
            if (it == null) {
                val server = getOrAdd(uri) { uuid -> BitbucketServer(uuid, uri) }
                lookupMap.put(BitbucketServerCoordinates(scheme, hostname, port, path), server.id)
                return server
            } else {
                return it
            }
        }
    }

    override fun lookup(scheme: UriScheme, hostname: String, port: Int, path: String): BitbucketServer? {
        return lookupMap[BitbucketServerCoordinates(scheme, hostname, port, path)]?.let { get(it) }
    }

    override fun getState(): Element {
        return Element("BitbucketServerRegistry").apply {
            addContent(super.getState())
            addContent(Element("lookup").apply {
                lookupMap.toSortedMap().forEach { e ->
                    addContent(Element("record").apply {
                        setAttribute("scheme", e.key.scheme.toString())
                        setAttribute("hostname", e.key.hostname)
                        setAttribute("port", e.key.port.toString())
                        setAttribute("path", e.key.path)
                        setAttribute("uuid", e.value.toString())
                    })
                }
            })
        }
    }

    override fun loadState(element: Element) {
        super.loadState(element, {
            val uuid = it.getUuid("uuid")
            val baseUri = it.getUri("baseUri")
            if (uuid != null && baseUri != null) BitbucketServer(uuid, baseUri) else null
        })

        lookupMap.clear()
        element.getChild("lookup").getChildren("record")
                .forEach {
                    val uriScheme = it.getUriScheme("scheme")
                    val hostname = it.getString("hostname")
                    val port = it.getInt("port")
                    val path = it.getString("path")
                    val uuid = it.getUuid("uuid")
                    if (uriScheme != null && hostname != null && port != null && path != null && uuid != null) {
                        lookupMap.put(BitbucketServerCoordinates(uriScheme, hostname, port, path), uuid)
                    }
                }
    }

    data class BitbucketServerCoordinates(val scheme: UriScheme = HTTP, val hostname: String, val port: Int, val path: String) : Comparable<BitbucketServerCoordinates> {
        override fun compareTo(other: BitbucketServerCoordinates) =
                Comparator.comparing(Function<BitbucketServerCoordinates, String> { t -> t.hostname })
                        .thenComparing { t -> t.path }
                        .thenComparing { t -> t.port }
                        .thenComparing { t -> t.scheme }
                        .compare(this, other)
    }
}

private fun Element.getString(attributeName: String): String? = this.getAttributeValue(attributeName)

private fun Element.getInt(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        if (it.isNotBlank()) it.toInt() else 0
    } catch(e: NumberFormatException) {
        log.warn("Malformed integer '$it' in the saved state")
        return null
    }
}

private fun Element.getUri(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        URI.create(it)
    } catch(e: IllegalArgumentException) {
        log.warn("Malformed URI '$it' in the saved state")
        return null
    }
}

private fun Element.getUriScheme(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        UriScheme.valueOf(it.toUpperCase())
    } catch(e: IllegalArgumentException) {
        log.warn("Unknown URI scheme '$it' in the saved state")
        return null
    }
}

private fun Element.getUuid(attributeName: String) = this.getAttributeValue(attributeName)?.let {
    try {
        UUID.fromString(it)
    } catch(e: IllegalArgumentException) {
        log.warn("Malformed UUID '$it' in the saved state")
        return null
    }
}
