@file:JvmName("HostingUtils")

package com.atlassian.bitbucket.linky.hosting

import com.atlassian.bitbucket.linky.UriScheme
import java.net.URI
import java.util.*

interface Hosting {
    val id: UUID
    val baseUri: URI
}

interface HostingRegistry<T : Hosting> {
    fun get(id: UUID): T?
    fun get(baseUri: URI): T?
    fun list(): List<T>
    fun remove(hosting: T)
}

data class BitbucketCloud(override val id: UUID, override val baseUri: URI) : Hosting

interface BitbucketCloudRegistry : HostingRegistry<BitbucketCloud> {
    fun getOrAdd(scheme: UriScheme, hostname: String, port: Int, uri: URI): BitbucketCloud
    fun lookup(scheme: UriScheme, hostname: String, port: Int): BitbucketCloud?
}

data class BitbucketServer(override val id: UUID, override val baseUri: URI) : Hosting

interface BitbucketServerRegistry : HostingRegistry<BitbucketServer> {
    fun getOrAdd(scheme: UriScheme, hostname: String, port: Int, path: String, uri: URI): BitbucketServer
    fun lookup(scheme: UriScheme, hostname: String, port: Int, path: String): BitbucketServer?
}
