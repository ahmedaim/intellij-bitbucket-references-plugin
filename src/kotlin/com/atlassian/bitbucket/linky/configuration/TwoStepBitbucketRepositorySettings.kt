package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.repository.BitbucketRepositorySettings
import com.atlassian.bitbucket.linky.repository.SettingKey

class TwoStepBitbucketRepositorySettings(private val delegate: BitbucketRepositorySettings) : BitbucketRepositorySettings {
    private val intermediateAdd: MutableMap<BitbucketRepository, MutableMap<SettingKey, String>> = mutableMapOf()
    private val intermediateDelete: MutableMap<BitbucketRepository, MutableSet<SettingKey>> = mutableMapOf()

    override fun getProperty(bitbucketRepository: BitbucketRepository, key: SettingKey, defaultValue: String?) =
            intermediateAdd[bitbucketRepository]?.get(key) ?: delegate.getProperty(bitbucketRepository, key, defaultValue)

    override fun setProperty(bitbucketRepository: BitbucketRepository, key: SettingKey, value: String) {
        var repoSettings = intermediateAdd[bitbucketRepository]
        if (repoSettings == null) {
            repoSettings = mutableMapOf()
            intermediateAdd.put(bitbucketRepository, repoSettings)
        }
        repoSettings.put(key, value)
    }

    override fun removeProperty(bitbucketRepository: BitbucketRepository, key: SettingKey) {
        intermediateAdd[bitbucketRepository]?.remove(key)

        var repoDeleteSettings = intermediateDelete[bitbucketRepository]
        if (repoDeleteSettings == null) {
            repoDeleteSettings = mutableSetOf()
            intermediateDelete.put(bitbucketRepository, repoDeleteSettings)
        }
        repoDeleteSettings.add(key)
    }

    fun isModified(): Boolean {
        return intermediateAdd.isNotEmpty() || intermediateDelete.isNotEmpty()
    }

    fun apply() {
        intermediateAdd.forEach {
            val repo = it.key
            it.value.forEach { delegate.setProperty(repo, it.key, it.value) }
        }
        intermediateAdd.clear()

        intermediateDelete.forEach {
            val repo = it.key
            it.value.forEach { delegate.removeProperty(repo, it) }
        }
        intermediateDelete.clear()
    }

    fun reset() {
        intermediateAdd.clear()
        intermediateDelete.clear()
    }
}
