package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.discovery.getRemoteUrls
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VfsUtil
import git4idea.GitUtil
import git4idea.repo.GitRepository
import org.apache.commons.lang.StringUtils
import org.zmlx.hg4idea.repo.HgRepository
import org.zmlx.hg4idea.util.HgUtil

interface RepositoriesTraverser {
    fun traverseRepositories(): Collection<Repository>
    fun <T> traverseRepositories(onAnyRepository: (Repository) -> T): Collection<T>
    fun <T> traverseRepositories(onGitRepository: (Repository) -> T, onHgRepository: (Repository) -> T): Collection<T>
}

class ProjectRepositoriesTraverser(private val project: Project) : RepositoriesTraverser {

    override fun traverseRepositories(): Collection<Repository> = traverseRepositories { it }

    override fun <T> traverseRepositories(onAnyRepository: (Repository) -> T) = traverseRepositories(onAnyRepository, onAnyRepository)

    override fun <T> traverseRepositories(onGitRepository: (Repository) -> T, onHgRepository: (Repository) -> T): Collection<T> {
        val gitItems = if (isClassAvailable("git4idea.commands.Git")) {
            GitUtil.getRepositoryManager(project)
                    .repositories
                    .map { onGitRepository.invoke(it) }
        } else {
            emptyList()
        }
        val hgItems = if (isClassAvailable("org.zmlx.hg4idea.HgVcs")) {
            HgUtil.getRepositoryManager(project)
                    .repositories
                    .map { onHgRepository.invoke(it) }
        } else {
            emptyList()
        }
        return gitItems + hgItems
    }
}

fun Repository.isGit() = this.vcs.keyInstanceMethod.name == "Git"
fun Repository.isHg() = this.vcs.keyInstanceMethod.name == "hg4idea"

private fun isClassAvailable(className: String): Boolean {
    try {
        Class.forName(className)
        return true
    } catch (e: ClassNotFoundException) {
        return false
    }
}

fun Repository.getRemoteUrls(): List<RemoteUrl> =
        if (this.isGit() && this is GitRepository) {
            this.getRemoteUrls()
        } else if (this.isHg() && this is HgRepository) {
            this.getRemoteUrls()
        } else {
            emptyList()
        }

fun Repository.getRelativePathInProject() = StringUtils.defaultIfEmpty(VfsUtil.getRelativePath(root, project.baseDir), "/")
