package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.Constants.LOGGER_NAME
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.HTTP
import com.atlassian.bitbucket.linky.UriScheme.HTTPS
import com.atlassian.bitbucket.linky.hosting.BitbucketCloud
import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.hosting.BitbucketServer
import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry
import com.atlassian.bitbucket.linky.repository.ProjectRepositoriesTraverser
import com.atlassian.bitbucket.linky.repository.getRemoteUrls
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.diagnostic.Logger
import rx.Observable
import java.net.URI
import java.util.regex.Matcher
import java.util.regex.Pattern

private val log = Logger.getInstance(LOGGER_NAME)

interface BitbucketRepositoriesDiscoveryService {
    fun discoverBitbucketRepositories(excludeRepositories: Collection<Repository> = emptyList()): Observable<RepositoryHostedOnBitbucket>
}

data class RepositoryHostedOnBitbucket(val repository: Repository, val bitbucketRepository: BitbucketRepository)

data class RemoteUrl(val scheme: UriScheme, val hostname: String, val port: Int, val path: String) {
    init {
        if (!path.startsWith("/")) {
            throw IllegalArgumentException("Path '$path' should start with a slash")
        }
        if (path.length > 1 && path.endsWith("/")) {
            throw IllegalArgumentException("Path '$path' should not end with a slash")
        }
    }
}

class DefaultBitbucketRepositoriesDiscoveryService(private val projectRepositoriesTraverser: ProjectRepositoriesTraverser,
                                                   private val cloudDiscoverer: CloudBitbucketRepositoryDiscoverer,
                                                   private val serverDiscoverer: ServerBitbucketRepositoryDiscoverer) : BitbucketRepositoriesDiscoveryService {

    override fun discoverBitbucketRepositories(excludeRepositories: Collection<Repository>): Observable<RepositoryHostedOnBitbucket> {
        return Observable
                .from(projectRepositoriesTraverser
                        .traverseRepositories()
                        .filter { it !in excludeRepositories })
                .flatMap { repository ->
                    val remoteUrls = repository.getRemoteUrls()
                    Observable.from(remoteUrls)
                            .flatMap {
                                Observable.concat(
                                        cloudDiscoverer.discover(repository, it),
                                        serverDiscoverer.discover(repository, it),
                                        cloudDiscoverer.discover(repository, it, active = true),
                                        serverDiscoverer.discover(repository, it, active = true))
                                        .first()
                                        .map { RepositoryHostedOnBitbucket(repository, it) }
                                        .onErrorResumeNext(Observable.empty())
                            }
                }
    }
}

interface BitbucketRepositoryDiscoverer {
    fun discover(repository: Repository, remoteUrl: RemoteUrl, active: Boolean = false): Observable<out BitbucketRepository>
    fun remoteUrlMatchesPattern(remoteUrl: RemoteUrl): Boolean
}

interface BitbucketRepositoryLinkingHelper {
    fun registerManuallyConfigured(remoteUrl: RemoteUrl, uri: URI)
    fun getRepositoryRelativeUrlPath(remoteUrl: RemoteUrl): String?
}

class CloudBitbucketRepositoryDiscoverer(private val cloudRegistry: BitbucketCloudRegistry,
                                         private val cloudProbe: BitbucketCloudProbe) : BitbucketRepositoryDiscoverer, BitbucketRepositoryLinkingHelper {
    private val pathPattern: Pattern = "/([^/]+)/([^/]+)".toPattern()

    override fun discover(repository: Repository, remoteUrl: RemoteUrl, active: Boolean): Observable<out BitbucketRepository> {
        return Observable.defer {
            val matcher = pathMatcher(remoteUrl)
            if (matcher.matches()) {
                val (scheme, host, port, path) = remoteUrl
                log.debug("Remote URL path '$path' matches Bitbucket Cloud repository path pattern")
                discoverCloudHosting(scheme, host, port, active)
                        .map {
                            val owner = matcher.group(1)
                            val slug = matcher.group(2)
                            BitbucketRepository.Cloud(repository, it, owner, slug)
                        }
            } else {
                Observable.empty()
            }
        }
    }

    override fun remoteUrlMatchesPattern(remoteUrl: RemoteUrl): Boolean = pathMatcher(remoteUrl).matches()

    override fun registerManuallyConfigured(remoteUrl: RemoteUrl, uri: URI) {
        register(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, uri)
    }

    override fun getRepositoryRelativeUrlPath(remoteUrl: RemoteUrl): String? {
        val matcher = pathMatcher(remoteUrl)
        if (matcher.matches()) {
            return "${matcher.group(1)}/${matcher.group(2)}"
        } else {
            return null
        }
    }

    private fun pathMatcher(remoteUrl: RemoteUrl): Matcher {
        return pathPattern.matcher(remoteUrl.path)
    }

    private fun discoverCloudHosting(scheme: UriScheme, host: String, port: Int, active: Boolean): Observable<BitbucketCloud> {
        var maybeCloud = cloudRegistry.lookup(scheme, host, port)
        if (maybeCloud == null && active) {
            maybeCloud = cloudProbe.probeBitbucketCloud(scheme, host, port)?.let { uri -> register(scheme, host, port, uri) }
        }

        return if (maybeCloud == null) Observable.empty() else Observable.just(maybeCloud)
    }

    private fun register(scheme: UriScheme, host: String, port: Int, uri: URI) =
            cloudRegistry.getOrAdd(scheme, host, port, uri)
}

class ServerBitbucketRepositoryDiscoverer(private val serverRegistry: BitbucketServerRegistry,
                                          private val serverProbe: BitbucketServerProbe) : BitbucketRepositoryDiscoverer, BitbucketRepositoryLinkingHelper {
    private val appPathPattern = "(/.+)?"
    private val fullSlugPattern = "([^/]+)/([^/]+)"
    private val httpPathPattern = "$appPathPattern/scm/$fullSlugPattern".toPattern()
    private val sshPathPattern = "$appPathPattern/$fullSlugPattern".toPattern()

    override fun discover(repository: Repository, remoteUrl: RemoteUrl, active: Boolean): Observable<out BitbucketRepository> {
        return Observable.defer {
            val (scheme, host, port, path) = remoteUrl
            val triple = parseRemoteUrlPath(remoteUrl)
            if (triple != null) {
                val (appPath, projectKey, slug) = triple
                log.debug("Remote URL path '$path' matches Bitbucket Server repository path pattern")
                discoverServerHosting(scheme, host, port, appPath, active)
                        .map { BitbucketRepository.Server(repository, it, projectKey, slug) }
            } else {
                Observable.empty()
            }
        }
    }

    override fun remoteUrlMatchesPattern(remoteUrl: RemoteUrl): Boolean = pathMatcher(remoteUrl).matches()

    override fun registerManuallyConfigured(remoteUrl: RemoteUrl, uri: URI) {
        parseRemoteUrlPath(remoteUrl)?.let {
            val appPath = it.first
            register(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath, uri)
        }
    }

    override fun getRepositoryRelativeUrlPath(remoteUrl: RemoteUrl): String? =
            parseRemoteUrlPath(remoteUrl)?.let {
                val (appPath, projectKey, slug) = it
                "projects/$projectKey/repos/$slug"
            }

    private fun pathMatcher(remoteUrl: RemoteUrl): Matcher {
        val pathPattern = if (remoteUrl.scheme in arrayOf(HTTP, HTTPS)) httpPathPattern else sshPathPattern
        return pathPattern.matcher(remoteUrl.path)
    }

    private fun parseRemoteUrlPath(remoteUrl: RemoteUrl): Triple<String, String, String>? {
        val matcher = pathMatcher(remoteUrl)
        return if (matcher.matches()) {
            Triple(matcher.group(1) ?: "/", matcher.group(2), matcher.group(3))
        } else null
    }

    private fun discoverServerHosting(scheme: UriScheme, host: String, port: Int, path: String, active: Boolean): Observable<BitbucketServer> {
        var maybeServer = serverRegistry.lookup(scheme, host, port, path)
        if (maybeServer == null && active) {
            maybeServer = serverProbe.probeBitbucketServer(scheme, host, port, path)?.let { uri -> register(scheme, host, port, path, uri) }
        }

        return if (maybeServer == null) Observable.empty() else Observable.just(maybeServer)
    }

    private fun register(scheme: UriScheme, host: String, port: Int, path: String, uri: URI) =
            serverRegistry.getOrAdd(scheme, host, port, path, uri)
}
