package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.cloud.rest.BitbucketCloudApi
import com.atlassian.bitbucket.linky.Constants.LOGGER_NAME
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.*
import com.intellij.openapi.diagnostic.Logger
import okhttp3.HttpUrl
import java.net.URI
import java.util.concurrent.TimeUnit

private val log = Logger.getInstance(LOGGER_NAME)

interface BitbucketCloudProbe {
    fun probeBitbucketCloud(detectedScheme: UriScheme,
                            detectedHost: String,
                            detectedPort: Int): URI?
}

class DefaultBitbucketCloudProbe : BitbucketCloudProbe {
    override fun probeBitbucketCloud(detectedScheme: UriScheme,
                                     detectedHost: String,
                                     detectedPort: Int): URI? {
        return candidates(detectedScheme, detectedHost, detectedPort)
                .find { isBitbucketCloudUrl(it) }?.uri()
    }

    private fun candidates(detectedScheme: UriScheme,
                           detectedHost: String,
                           detectedPort: Int): List<HttpUrl> =
            listOf(HTTPS, HTTP)
                    .filter { if (detectedScheme in arrayOf(SSH, GIT)) true else it == detectedScheme }
                    .flatMap { scheme ->
                        listOf(-1, detectedPort)
                                .distinct()
                                .map { port ->
                                    val urlBuilder = HttpUrl.Builder()
                                            .scheme(scheme.toString())
                                            .host(detectedHost)
                                    if (port > 0) {
                                        urlBuilder.port(port)
                                    }
                                    urlBuilder.build()
                                }
                    }

    private fun isBitbucketCloudUrl(url: HttpUrl): Boolean {
        try {
            log.debug("Probing for Bitbucket Cloud at '$url")
            return BitbucketCloudApi.guest(url.toString()).testConnectivity().await(3, TimeUnit.SECONDS)
        } catch(e: RuntimeException) {
            log.debug("Error while checking Bitbucket Cloud candidate url '${url.toString()}", e)
            return false
        }
    }
}
