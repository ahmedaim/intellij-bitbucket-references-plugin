package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.Constants.LOGGER_NAME
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.AbstractProjectComponent
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.ProjectLevelVcsManager
import com.intellij.openapi.vcs.VcsListener
import com.intellij.openapi.vfs.VirtualFile
import rx.Completable
import java.time.Duration
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

private val log = Logger.getInstance(LOGGER_NAME)

interface BitbucketRepositoriesService {
    fun listBitbucketRepositories(): List<BitbucketRepository>
    fun getBitbucketRepository(rootDirectory: VirtualFile): BitbucketRepository?
    fun configurationChanged(awaitTimeout: Duration? = null)
}

class DefaultBitbucketRepositoriesService(private val project: Project,
                                          private val discoveryService: BitbucketRepositoriesDiscoveryService) :
        AbstractProjectComponent(project), BitbucketRepositoriesService, VcsListener {
    private val discoveryDelay = Duration.ofMinutes(10)
    private val repositories: MutableMap<Repository, BitbucketRepository> = ConcurrentHashMap(8)
    private val discoveryExecutor = Executors.newSingleThreadScheduledExecutor(
            ThreadFactoryBuilder()
                    .setNameFormat("bitbucket-reference-plugin-remote-discoverer-%d")
                    .setDaemon(true)
                    .build())
    private val discoveryFinished = AtomicInteger(0)
    private val discoveryTask = Runnable {
        val seenFinishedCount = discoveryFinished.get()
        // exclude already resolved repositories
        discoveryService.discoverBitbucketRepositories(repositories.keys)
                .doOnCompleted { discoveryFinished.compareAndSet(seenFinishedCount, seenFinishedCount + 1) }
                .subscribe(
                        { repo -> repositories.put(repo.repository, repo.bitbucketRepository) },
                        { error ->
                            if (error is NoSuchElementException) {
                                log.info("No Bitbucket repositories discovered in the project", error)
                            } else {
                                log.error("Error while discovering Bitbucket repositories", error)
                            }
                        })
    }

    private var discoveryTaskFuture: Future<Any>? = null

    override fun initComponent() {
        project.messageBus.connect()
                .subscribe(ProjectLevelVcsManager.VCS_CONFIGURATION_CHANGED, this);
    }

    override fun listBitbucketRepositories(): List<BitbucketRepository> =
            repositories.values
                    .sortedBy { it.repository.root.path }
                    .toList()

    override fun getBitbucketRepository(rootDirectory: VirtualFile) =
            repositories.entries.find { it.key.root == rootDirectory }?.value

    override fun configurationChanged(awaitTimeout: Duration?) {
        discoveryTaskFuture?.cancel(true)
        repositories.clear()
        val seenFinishedCount = discoveryFinished.get()
        discoveryExecutor.scheduleAtFixedRate(discoveryTask, 0, discoveryDelay.toMinutes(), TimeUnit.MINUTES);
        if (awaitTimeout != null) {
            Completable.fromAction {
                while (true) {
                    if (discoveryFinished.get() > seenFinishedCount) {
                        break
                    }
                }
            }.await(awaitTimeout.toMillis(), TimeUnit.MILLISECONDS)
        }
    }

    override fun directoryMappingChanged() {
        configurationChanged()
    }
}
