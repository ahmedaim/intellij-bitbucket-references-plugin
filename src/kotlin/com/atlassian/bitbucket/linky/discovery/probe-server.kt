package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.Constants.LOGGER_NAME
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.*
import com.atlassian.bitbucket.server.rest.BitbucketServerApi
import com.intellij.openapi.diagnostic.Logger
import okhttp3.HttpUrl
import java.net.URI
import java.util.concurrent.TimeUnit

private val log = Logger.getInstance(LOGGER_NAME)

interface BitbucketServerProbe {
    fun probeBitbucketServer(detectedScheme: UriScheme,
                             detectedHost: String,
                             detectedPort: Int,
                             detectedApplicationPath: String): URI?
}

class DefaultBitbucketServerProbe : BitbucketServerProbe {
    override fun probeBitbucketServer(detectedScheme: UriScheme,
                                      detectedHost: String,
                                      detectedPort: Int,
                                      detectedApplicationPath: String): URI? {
        val appPath = if (detectedApplicationPath.isEmpty()) "/" else detectedApplicationPath
        return candidates(detectedScheme, detectedHost, detectedPort, appPath)
                .find { isBitbucketServerUrl(it) }?.uri()
    }

    private fun candidates(detectedScheme: UriScheme,
                           detectedHost: String,
                           detectedPort: Int,
                           detectedPath: String): List<HttpUrl> =
            listOf(HTTPS, HTTP)
                    .filter { if (detectedScheme in arrayOf(SSH, GIT)) true else it == detectedScheme }
                    .flatMap { scheme ->
                        listOf(-1, detectedPort)
                                .distinct()
                                .map { port ->
                                    val urlBuilder = HttpUrl.Builder()
                                            .scheme(scheme.toString())
                                            .host(detectedHost)
                                            .encodedPath(detectedPath)
                                    if (port > 0) {
                                        urlBuilder.port(port)
                                    }
                                    urlBuilder.build()
                                }
                    }

    private fun isBitbucketServerUrl(url: HttpUrl): Boolean {
        try {
            log.debug("Probing for Bitbucket Server at '$url")
            return BitbucketServerApi.guest(url.toString()).testConnectivity().await(3, TimeUnit.SECONDS)
        } catch(e: RuntimeException) {
            log.debug("Error while checking Bitbucket Server candidate url '${url.toString()}", e)
            return false
        }
    }
}
