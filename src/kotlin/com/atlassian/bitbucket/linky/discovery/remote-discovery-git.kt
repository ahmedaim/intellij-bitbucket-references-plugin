package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.Constants.LOGGER_NAME
import com.atlassian.bitbucket.linky.UriScheme
import com.intellij.openapi.diagnostic.Logger
import git4idea.GitUtil
import git4idea.repo.GitRemote
import git4idea.repo.GitRepository
import java.net.URI
import java.net.URISyntaxException
import java.util.*

private val log = Logger.getInstance(LOGGER_NAME)
private val remoteUrlParser = GitRemoteUrlParser()

fun GitRepository.getRemoteUrls(): List<RemoteUrl> =
        getSortedRemotes()
                .flatMap { it.urls }
                .distinct()
                .map { remoteUrlParser.parseRemoteUrl(it) }
                .filterNotNull()

private fun GitRepository.getMainRemote(): Optional<GitRemote> {
    val trackInfo = GitUtil.getTrackInfoForCurrentBranch(this)
    if (trackInfo != null) {
        return Optional.of(trackInfo.remote)
    }

    val defaultRemote = GitUtil.getDefaultRemote(remotes)
    if (defaultRemote != null) {
        return Optional.of(defaultRemote)
    }

    return Optional.ofNullable(remotes.firstOrNull())
}

private fun GitRepository.getSortedRemotes(): List<GitRemote> {
    return getMainRemote()
            .map { mainRemote -> listOf(mainRemote) + remotes.filterNot { it == mainRemote } }
            .orElse(ArrayList(remotes))
}

private class GitRemoteUrlParser {
    fun parseRemoteUrl(url: String): RemoteUrl? {
        var urlToParse = url.toLowerCase()
        if (!urlToParse.contains("://")) {
            urlToParse = "ssh://$urlToParse"
        }
        try {
            return parseRemoteCoordinates(URI(urlToParse))
        } catch(e: URISyntaxException) {
            log.warn("Failed to parse URI '$url' -> '$urlToParse'")
            return null
        }
    }

    private fun parseRemoteCoordinates(uri: URI): RemoteUrl? {
        val maybeScheme = UriScheme.forName(uri.scheme)

        if (!maybeScheme.isPresent) {
            log.warn("Unknown scheme '${uri.scheme}' in the URI '$uri'")
            return null
        }

        val scheme = maybeScheme.get()
        var host = uri.host
        val port = uri.port
        val authority = uri.authority
        var path = uri.path?.removeSuffix(".git") ?: ""

        // host might be null for Bitbucket Cloud SSH URLs
        if (host == null && authority != null) {
            val colonIndex = authority.lastIndexOf(':')
            host = authority.substring(authority.indexOf('@') + 1, colonIndex)
            path = authority.substring(colonIndex + 1) + path
        }

        path = path.removeSuffix("/")
        if (!path.startsWith("/")) {
            path = "/$path"
        }

        if (host == null) {
            log.debug("No hostname parsed from the URI '$uri'")
            return null
        }

        return RemoteUrl(scheme, host, port, path)
    }
}
