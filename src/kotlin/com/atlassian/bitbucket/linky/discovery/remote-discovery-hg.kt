package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.Constants.LOGGER_NAME
import com.atlassian.bitbucket.linky.UriScheme
import com.intellij.openapi.diagnostic.Logger
import org.zmlx.hg4idea.repo.HgRepository
import java.net.URI
import java.net.URISyntaxException

private val log = Logger.getInstance(LOGGER_NAME)
private val remoteUrlParser: HgRemoteUrlParser = HgRemoteUrlParser()

fun HgRepository.getRemoteUrls(): List<RemoteUrl> =
        getSortedRemotes()
                .distinct()
                .map { remoteUrlParser.parseRemoteUrl(it) }
                .filterNotNull()

private fun HgRepository.getSortedRemotes(): List<String> =
        repositoryConfig.defaultPath.let { mainRemote ->
            listOf(mainRemote)
                    .plus(repositoryConfig.paths.filterNot { it == mainRemote })
                    .filterNotNull()
        }

internal class HgRemoteUrlParser {

    fun parseRemoteUrl(url: String): RemoteUrl? {
        val urlToParse = url.toLowerCase()
        try {
            return parseRemoteCoordinates(URI(urlToParse))
        } catch(e: URISyntaxException) {
            log.warn("Failed to parse URI '$url' -> '$urlToParse'")
            return null
        }
    }

    private fun parseRemoteCoordinates(uri: URI): RemoteUrl? {
        val maybeScheme = UriScheme.forName(uri.scheme)

        if (!maybeScheme.isPresent) {
            log.warn("Unknown scheme '${uri.scheme}' in the URI '$uri'")
            return null
        }

        val scheme = maybeScheme.get()
        val host = uri.host
        val port = uri.port
        var path = uri.path ?: ""

        if (host == null) {
            log.debug("No hostname parsed from the URI '$uri'")
            return null
        }

        path = path.removeSuffix("/")
        if (!path.startsWith("/")) {
            path = "/$path"
        }

        return RemoteUrl(scheme, host, port, path)
    }
}
