package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.blame.BlameLineReferenceProvider
import com.atlassian.bitbucket.linky.repository.BitbucketRepositorySettings
import com.atlassian.bitbucket.linky.repository.SettingKey
import com.atlassian.bitbucket.linky.util.LineSelections.collectLinesSelections
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import java.net.URI
import java.util.*

class CloudLinky(private val cloud: BitbucketRepository.Cloud,
                 private val settings: BitbucketRepositorySettings,
                 private val blameLineReferenceProvider: BlameLineReferenceProvider) : BitbucketLinky {
    private val sourceViewRelativePath = "src"

    override fun getBaseUri(): URI = cloud.baseUri.uri()

    override fun getCommitViewUri(commitReference: String, file: VirtualFile, lineNumber: Int): URI =
            cloud.baseUri.newBuilder()
                    .addPathSegment("commits")
                    .addPathSegment(commitReference)
                    .let { urlBuilder ->
                        blameLineReferenceProvider.getBlameLineReference(file, lineNumber)
                                .map { urlBuilder.fragment("L${it.first}T${it.second}") }
                                .orElse(urlBuilder)
                    }.build().uri()

    override fun getSourceViewUri(): URI = cloud.baseUri.newBuilder().addPathSegment(sourceViewRelativePath).build().uri()

    override fun getSourceViewUri(file: VirtualFile, linesSelections: List<LinesSelection>): URI {
        val relativeFilePath = VfsUtil.getRelativePath(file, cloud.repository.root)
        val urlBuilder = cloud.baseUri.newBuilder()
                .addPathSegment(sourceViewRelativePath)
                .addPathSegment(cloud.repository.currentVcsReference())
                .addPathSegments(relativeFilePath)

        val fragment = collectLinesSelections(linesSelections, file.name + "-", ",", ":", "")
        fragment.ifPresent { urlBuilder.fragment(it) }

        return urlBuilder.build().uri()
    }

    override fun getPullRequestUri(): Optional<URI> =
            cloud.repository.currentBranchName.let {
                Optional.ofNullable(it)
                        .map {
                            cloud.baseUri.newBuilder()
                                    .addPathSegment("pull-requests")
                                    .addPathSegment("new")
                                    .addQueryParameter("source", it)
                                    .apply {
                                        settings.getProperty(cloud, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME)?.let {
                                            addQueryParameter("dest", it)
                                        }
                                    }
                                    .build().uri()
                        }
            }
}

class ServerLinky(private val server: BitbucketRepository.Server,
                  private val settings: BitbucketRepositorySettings,
                  private val blameLineReferenceProvider: BlameLineReferenceProvider) : BitbucketLinky {
    private val sourceViewRelativePath = "browse"

    override fun getBaseUri(): URI = server.baseUri.uri()

    override fun getCommitViewUri(commitReference: String, file: VirtualFile, lineNumber: Int): URI =
            server.baseUri.newBuilder()
                    .addPathSegment("commits")
                    .addPathSegment(commitReference)
                    .let { urlBuilder ->
                        blameLineReferenceProvider.getBlameLineReference(file, lineNumber)
                                // Bitbucket Server doesn't support line selection in the commit view
                                .map { urlBuilder.fragment(it.first) }
                                .orElse(urlBuilder)
                    }.build().uri()

    override fun getSourceViewUri(): URI = server.baseUri.newBuilder().addPathSegment(sourceViewRelativePath).build().uri()

    override fun getSourceViewUri(file: VirtualFile, linesSelections: List<LinesSelection>): URI {
        val relativeFilePath = VfsUtil.getRelativePath(file, server.repository.root)
        val urlBuilder = server.baseUri.newBuilder()
                .addPathSegment(sourceViewRelativePath)
                .addPathSegments(relativeFilePath)
                .addQueryParameter("at", server.repository.currentVcsReference())

        val fragment = collectLinesSelections(linesSelections, "", ",", "-", "")
        fragment.ifPresent { urlBuilder.fragment(it) }

        return urlBuilder.build().uri()
    }

    override fun getPullRequestUri(): Optional<URI> {
        val branchName = server.repository.currentBranchName
        return Optional.ofNullable(branchName)
                .map {
                    server.baseUri.newBuilder()
                            .addPathSegments("pull-requests")
                            .query("create")
                            .addQueryParameter("sourceBranch", it)
                            .apply {
                                settings.getProperty(server, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME)?.let {
                                    addQueryParameter("targetBranch", it)
                                }
                            }
                            .build().uri()
                }
    }
}

private fun Repository.currentVcsReference() = currentRevision ?: currentBranchName ?: ""
