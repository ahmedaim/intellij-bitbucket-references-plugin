package com.atlassian.bitbucket.linky.actions

import com.atlassian.bitbucket.linky.BitbucketLinky
import com.atlassian.bitbucket.linky.action.ActionUtil
import com.atlassian.bitbucket.linky.action.reference.AbstractBitbucketReferenceAction
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MessageType
import com.intellij.openapi.vcs.AbstractVcs
import com.intellij.openapi.vcs.annotate.AnnotationGutterActionProvider
import com.intellij.openapi.vcs.annotate.FileAnnotation
import com.intellij.openapi.vcs.annotate.LineNumberListener
import com.intellij.openapi.vcs.history.VcsRevisionNumber
import com.intellij.openapi.vcs.impl.UpToDateLineNumberProviderImpl
import com.intellij.openapi.vfs.VirtualFile
import icons.BitbucketLinkyIcons.Actions.CopyBitbucketUrl
import icons.BitbucketLinkyIcons.Actions.OpenInBitbucket
import java.awt.datatransfer.StringSelection
import java.net.URI
import javax.swing.Icon

class CommitLinkGutterAction(text: String,
                             description: String,
                             icon: Icon,
                             private val annotation: FileAnnotation,
                             private val commitLinkAction: (Project, URI) -> Unit) : LineNumberListener,
        AbstractBitbucketReferenceAction(text, description, icon) {
    private var lineNumber: Int = -1

    override fun consume(lineNumber: Int) {
        this.lineNumber = lineNumber
    }

    override fun actionPerformed(event: AnActionEvent,
                                 project: Project,
                                 file: VirtualFile,
                                 linky: BitbucketLinky,
                                 vcs: AbstractVcs<*>) {
        val actualLineNumber = getActualLineNumber(project, file, lineNumber)
        if (actualLineNumber >= 0) {
            val revisionNumber = annotation.getLineRevisionNumber(actualLineNumber)
            if (revisionNumber != null) {
                val uri = linky.getCommitViewUri(prepareRevisionHash(revisionNumber), file, actualLineNumber + 1)
                commitLinkAction.invoke(project, uri)
            }
        }
    }

    override fun couldActionBeEnabled(project: Project, file: VirtualFile): Boolean =
            getActualLineNumber(project, file, lineNumber) >= 0

    private fun getActualLineNumber(project: Project, file: VirtualFile, lineNumber: Int) =
            FileDocumentManager.getInstance().getDocument(file)?.let {
                UpToDateLineNumberProviderImpl(it, project).getLineNumber(lineNumber)
            } ?: -1

    private fun prepareRevisionHash(vcsRevisionNumber: VcsRevisionNumber) =
            vcsRevisionNumber.asString()
                    // hg revision hash is prefixed with revision number separated by colon
                    .substringAfter(':')
}

class OpenCommitInBitbucketGutterActionProvider : AnnotationGutterActionProvider {
    override fun createAction(annotation: FileAnnotation): AnAction =
            CommitLinkGutterAction("Open Commit in Bitbucket", "Open selected Commit in Bitbucket", OpenInBitbucket, annotation)
            { project, uri ->
                ActionUtil.openUriInBrowser(project, uri)
            }
}

class CopyCommitLinkGutterActionProvider : AnnotationGutterActionProvider {
    override fun createAction(annotation: FileAnnotation): AnAction =
            CommitLinkGutterAction("Copy Bitbucket Commit link", "Copy URL for Bitbucket Commit view of the selected commit", CopyBitbucketUrl, annotation)
            { project, uri ->
                val toCopy = uri.toASCIIString()
                CopyPasteManager.getInstance().setContents(StringSelection(toCopy));
                ActionUtil.showStatusBarPopupMessage(project, MessageType.INFO, toCopy + " has been copied");
            }
}
