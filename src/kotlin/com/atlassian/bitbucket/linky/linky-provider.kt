package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.blame.BlameLineService
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.atlassian.bitbucket.linky.repository.BitbucketRepositorySettings
import com.intellij.openapi.vcs.ProjectLevelVcsManager
import com.intellij.openapi.vfs.VirtualFile
import java.util.*

// TODO null vs Optional
interface BitbucketLinkyProvider {
    fun getBitbucketLinky(file: VirtualFile): Optional<BitbucketLinky>
}

class DefaultBitbucketLinkyProvider(private val vcsManager: ProjectLevelVcsManager,
                                    private val bitbucketRepositoriesService: BitbucketRepositoriesService,
                                    private val bitbucketRepositorySettings: BitbucketRepositorySettings,
                                    private val blameLineService: BlameLineService) : BitbucketLinkyProvider {

    override fun getBitbucketLinky(file: VirtualFile): Optional<BitbucketLinky> {
        return Optional.ofNullable(
                vcsManager.getVcsRootFor(file)?.let {
                    bitbucketRepositoriesService.getBitbucketRepository(it)
                }?.let {
                    val blameLineReferenceProvider = blameLineService.getBlameLineReferenceProvider(it)
                    when (it) {
                        is BitbucketRepository.Cloud -> CloudLinky(it, bitbucketRepositorySettings, blameLineReferenceProvider)
                        is BitbucketRepository.Server -> ServerLinky(it, bitbucketRepositorySettings, blameLineReferenceProvider)
                    }
                })
    }
}
