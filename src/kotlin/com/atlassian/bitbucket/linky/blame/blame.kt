package com.atlassian.bitbucket.linky.blame

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.intellij.openapi.components.ServiceManager
import com.intellij.openapi.project.Project

interface BlameLineService {
    fun getBlameLineReferenceProvider(bitbucketRepository: BitbucketRepository): BlameLineReferenceProvider
}

class DefaultBlameLineService(private val project: Project) : BlameLineService {
    override fun getBlameLineReferenceProvider(bitbucketRepository: BitbucketRepository): BlameLineReferenceProvider {
        if (bitbucketRepository.isGit()) {
            return ServiceManager.getService(project, GitBlameLineReferenceProvider::class.java)
        }
        if (bitbucketRepository.isHg()) {
            return ServiceManager.getService(project, HgBlameLineReferenceProvider::class.java)
        }
        throw IllegalArgumentException("Unknown VCS of Bitbucket repository $bitbucketRepository")
    }
}
