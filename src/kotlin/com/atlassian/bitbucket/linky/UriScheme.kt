package com.atlassian.bitbucket.linky

import java.util.*

enum class UriScheme constructor(private val presentation: String) {
    GIT("git"),
    HTTP("http"),
    HTTPS("https"),
    SSH("ssh");

    override fun toString(): String {
        return presentation
    }

    companion object {
        fun forName(presentation: String?): Optional<UriScheme> {
            return Arrays.stream(values())
                    .filter { it.presentation.equals(presentation, ignoreCase = true) }
                    .findAny()
        }
    }
}
