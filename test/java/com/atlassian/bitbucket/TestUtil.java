package com.atlassian.bitbucket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URL;

public class TestUtil {

    private TestUtil() {
    }

    public static String readResourceAsString(Class clazz, String resourcePath) {
        URL url = Resources.getResource(clazz, resourcePath);
        try {
            return Resources.toString(url, Charsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to get resource '%s'", url), e);
        }
    }

    public static JsonNode readResourceAsJson(Class clazz, String resourcePath) {
        String text = readResourceAsString(clazz, resourcePath);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(text);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to parse JSON from '%s'", text), e);
        }
    }
}
