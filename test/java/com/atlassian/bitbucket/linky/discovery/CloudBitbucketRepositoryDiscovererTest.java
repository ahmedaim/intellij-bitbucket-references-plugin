package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.BitbucketRepository;
import com.atlassian.bitbucket.linky.hosting.BitbucketCloud;
import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry;
import com.intellij.dvcs.repo.Repository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.URI;
import java.util.UUID;
import java.util.function.Function;

import static com.atlassian.bitbucket.linky.UriScheme.HTTPS;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class CloudBitbucketRepositoryDiscovererTest {
    private static final URI TEST_URI = java.net.URI.create("http://example.com");
    private static final BitbucketCloud TEST_CLOUD = new BitbucketCloud(UUID.randomUUID(), TEST_URI);

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private BitbucketCloudRegistry registry;
    @Mock
    private Repository repository;
    @Mock
    private BitbucketCloudProbe probe;
    @InjectMocks
    private CloudBitbucketRepositoryDiscoverer discoverer;

    @Before
    public void setUp() throws Exception {
        when(probe.probeBitbucketCloud(any(), anyString(), anyInt())).thenReturn(TEST_URI);
        when(registry.getOrAdd(any(), anyString(), anyInt(), any())).thenReturn(TEST_CLOUD);
    }

    @Test
    public void testDiscoveryIsDeferred() {
        RemoteUrl remoteUrl = new RemoteUrl(HTTPS, "bitbucket.org", -1, "/bitbucket/test");
        discoverer.discover(this.repository, remoteUrl, true);

        verifyZeroInteractions(registry);
        verifyZeroInteractions(probe);
    }

    @Test
    public void testRepositoryCreation() {
        RemoteUrl remoteUrl = new RemoteUrl(HTTPS, "bitbucket.org", -1, "/bitbucket/test");
        BitbucketRepository repo = discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        assertThat(repo, instanceOf(BitbucketRepository.Cloud.class));
        BitbucketRepository.Cloud cloudRepo = (BitbucketRepository.Cloud) repo;
        assertThat(cloudRepo.getRepository(), is(repository));
        assertThat(cloudRepo.getHosting(), is(TEST_CLOUD));
        assertThat(cloudRepo.getOwner(), is("bitbucket"));
        assertThat(cloudRepo.getSlug(), is("test"));
    }

    @Test
    public void shouldNotProbeIfHostingFoundInRegistry() {
        when(registry.lookup(any(), anyString(), anyInt())).thenReturn(TEST_CLOUD);
        RemoteUrl remoteUrl = new RemoteUrl(HTTPS, "bitbucket.org", -1, "/bitbucket/test");

        discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()));
        verifyZeroInteractions(probe);
        verify(registry, never()).getOrAdd(any(), anyString(), anyInt(), any());
    }

    @Test
    public void shouldNotProbeIfNotActiveDiscovery() {
        RemoteUrl remoteUrl = new RemoteUrl(HTTPS, "bitbucket.org", -1, "/bitbucket/test");
        BitbucketRepository repository = discoverer.discover(this.repository, remoteUrl, false)
                .defaultIfEmpty(null)
                .toBlocking().single();

        assertThat(repository, nullValue());

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()));
        verifyZeroInteractions(probe);
    }

    @Test
    public void shouldNotRegisterIfProbeFoundNothing() {
        when(probe.probeBitbucketCloud(any(), anyString(), anyInt())).thenReturn(null);
        RemoteUrl remoteUrl = new RemoteUrl(HTTPS, "bitbucket.org", -1, "/bitbucket/test");

        BitbucketRepository repo = discoverer.discover(this.repository, remoteUrl, true)
                .defaultIfEmpty(null)
                .toBlocking().single();

        assertThat(repo, nullValue());

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()));
        verify(probe).probeBitbucketCloud(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()));
        verify(registry, never()).getOrAdd(any(), anyString(), anyInt(), any());
    }

    @Test
    public void shouldRegisterIfProbeFoundBitbucket() {
        RemoteUrl remoteUrl = new RemoteUrl(HTTPS, "bitbucket.org", -1, "/bitbucket/test");

        discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()));
        verify(probe).probeBitbucketCloud(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()));
        verify(registry).getOrAdd(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), any());
    }

    @Test
    public void shouldSkipRemotesIfPathUnknown() {
        Function<String, BitbucketRepository> discoverRepoForRemoteWithPath = path ->
                discoverer.discover(this.repository, new RemoteUrl(HTTPS, "example.com", -1, path), true)
                        .defaultIfEmpty(null)
                        .toBlocking().single();

        assertThat(discoverRepoForRemoteWithPath.apply("/three/segments/path"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("/short_path"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("/a//b"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("///a"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("//a"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("/"), nullValue());

        verifyZeroInteractions(registry);
        verifyZeroInteractions(probe);
    }
}
