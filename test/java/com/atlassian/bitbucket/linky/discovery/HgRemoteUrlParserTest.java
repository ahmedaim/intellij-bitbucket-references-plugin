package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.UriScheme;
import org.hamcrest.Matcher;
import org.junit.Test;

import static com.atlassian.bitbucket.linky.UriScheme.*;
import static com.atlassian.bitbucket.linky.UriScheme.SSH;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class HgRemoteUrlParserTest {

    private HgRemoteUrlParser parser = new HgRemoteUrlParser();

    @Test
    public void testCloudRemoteUrl() {
        assertUrlParsed("ssh://hg@bitbucket.org/atlas/test",
                is(SSH), is("bitbucket.org"), is(-1), is("/atlas/test"));
        assertUrlParsed("https://dpenkin@bitbucket.org/atlas/test",
                is(HTTPS), is("bitbucket.org"), is(-1), is("/atlas/test"));
    }

    @Test
    public void testParseOtherRemoteUrls() {
        assertUrlParsed("http://example",
                is(HTTP), is("example"), is(-1), is("/"));
        assertUrlParsed("http://example.com",
                is(HTTP), is("example.com"), is(-1), is("/"));
        assertUrlParsed("http://example.com/",
                is(HTTP), is("example.com"), is(-1), is("/"));
        assertUrlParsed("http://example.com:239",
                is(HTTP), is("example.com"), is(239), is("/"));
        assertUrlParsed("http://example.com:239/",
                is(HTTP), is("example.com"), is(239), is("/"));
        assertUrlParsed("http://example.com:239/hello/world/",
                is(HTTP), is("example.com"), is(239), is("/hello/world"));
    }

    @Test
    public void testParseOtherSchemesReturnsNull() {
        assertThat(parser.parseRemoteUrl("file://whatever"), nullValue());
        assertThat(parser.parseRemoteUrl("example"), nullValue());
    }

    private void assertUrlParsed(String urlToParse,
                                 Matcher<UriScheme> schemeMatcher,
                                 Matcher<String> hostnameMatcher,
                                 Matcher<Integer> portMatcher,
                                 Matcher<String> pathMatcher) {
        RemoteUrl url = parser.parseRemoteUrl(urlToParse);
        assertThat(url, notNullValue());
        assertThat(url.getScheme(), schemeMatcher);
        assertThat(url.getHostname(), hostnameMatcher);
        assertThat(url.getPort(), portMatcher);
        assertThat(url.getPath(), pathMatcher);
    }
}
