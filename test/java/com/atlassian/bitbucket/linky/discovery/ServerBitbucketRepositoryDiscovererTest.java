package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.BitbucketRepository;
import com.atlassian.bitbucket.linky.hosting.BitbucketServer;
import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry;
import com.intellij.dvcs.repo.Repository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.URI;
import java.util.UUID;
import java.util.function.Function;

import static com.atlassian.bitbucket.linky.UriScheme.HTTPS;
import static com.atlassian.bitbucket.linky.UriScheme.SSH;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ServerBitbucketRepositoryDiscovererTest {
    private static final URI TEST_URI = URI.create("http://example.com");
    private static final BitbucketServer TEST_SERVER = new BitbucketServer(UUID.randomUUID(), TEST_URI);

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private BitbucketServerRegistry registry;
    @Mock
    private Repository repository;
    @Mock
    private BitbucketServerProbe probe;
    @InjectMocks
    private ServerBitbucketRepositoryDiscoverer discoverer;

    @Before
    public void setUp() throws Exception {
        when(probe.probeBitbucketServer(any(), anyString(), anyInt(), anyString())).thenReturn(TEST_URI);
        when(registry.getOrAdd(any(), anyString(), anyInt(), anyString(), any())).thenReturn(TEST_SERVER);
    }

    @Test
    public void testDiscoveryIsDeferred() {
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/bitbucket/test");
        discoverer.discover(this.repository, remoteUrl, true);

        verifyZeroInteractions(registry);
        verifyZeroInteractions(probe);
    }

    @Test
    public void testRepositoryCreation() {
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/bitbucket/test");
        BitbucketRepository repo = discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        assertThat(repo, instanceOf(BitbucketRepository.Server.class));
        BitbucketRepository.Server serverRepo = (BitbucketRepository.Server) repo;
        assertThat(serverRepo.getRepository(), is(repository));
        assertThat(serverRepo.getHosting(), is(TEST_SERVER));
        assertThat(serverRepo.getProjectKey(), is("bitbucket"));
        assertThat(serverRepo.getSlug(), is("test"));
    }

    @Test
    public void shouldNotProbeIfHostingFoundInRegistry() {
        when(registry.lookup(any(), anyString(), anyInt(), anyString())).thenReturn(TEST_SERVER);
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/bitbucket/test");

        discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/"));
        verifyZeroInteractions(probe);
        verify(registry, never()).getOrAdd(any(), anyString(), anyInt(), anyString(), any());
    }

    @Test
    public void shouldNotProbeIfNotActiveDiscovery() {
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/bitbucket/test");
        BitbucketRepository repository = discoverer.discover(this.repository, remoteUrl, false)
                .defaultIfEmpty(null)
                .toBlocking().single();

        assertThat(repository, nullValue());

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/"));
        verifyZeroInteractions(probe);
    }

    @Test
    public void shouldNotRegisterIfProbeFoundNothing() {
        when(probe.probeBitbucketServer(any(), anyString(), anyInt(), anyString())).thenReturn(null);
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/bitbucket/test");

        BitbucketRepository repo = discoverer.discover(this.repository, remoteUrl, true)
                .defaultIfEmpty(null)
                .toBlocking().single();

        assertThat(repo, nullValue());

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/"));
        verify(probe).probeBitbucketServer(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/"));
        verify(registry, never()).getOrAdd(any(), anyString(), anyInt(), anyString(), any());
    }

    @Test
    public void shouldRegisterIfProbeFoundBitbucket() {
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/bitbucket/test");

        discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/"));
        verify(probe).probeBitbucketServer(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/"));
        verify(registry).getOrAdd(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/"), any());
    }

    @Test
    public void shouldFindApplicationPath() {
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/path/bitbucket/test");

        discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/path"));
    }

    @Test
    public void shouldIgnoreScmPathSegmentForSshUrls() {
        RemoteUrl remoteUrl = new RemoteUrl(SSH, "stash.atlassian.com", -1, "/path/scm/bitbucket/test");

        BitbucketRepository repo = discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        assertThat(repo, instanceOf(BitbucketRepository.Server.class));
        BitbucketRepository.Server serverRepo = (BitbucketRepository.Server) repo;
        assertThat(serverRepo.getProjectKey(), is("bitbucket"));
        assertThat(serverRepo.getSlug(), is("test"));

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/path/scm"));
    }

    @Test
    public void shouldOmitScmPathSegmentForHttpUrls() {
        RemoteUrl remoteUrl = new RemoteUrl(HTTPS, "stash.atlassian.com", -1, "/path/scm/bitbucket/test");

        BitbucketRepository repo = discoverer.discover(this.repository, remoteUrl, true).toBlocking().single();

        assertThat(repo, instanceOf(BitbucketRepository.Server.class));
        BitbucketRepository.Server serverRepo = (BitbucketRepository.Server) repo;
        assertThat(serverRepo.getProjectKey(), is("bitbucket"));
        assertThat(serverRepo.getSlug(), is("test"));

        verify(registry).lookup(eq(remoteUrl.getScheme()), eq(remoteUrl.getHostname()), eq(remoteUrl.getPort()), eq("/path"));
    }

    @Test
    public void shouldSkipRemotesIfPathUnknown() {
        Function<String, BitbucketRepository> discoverRepoForRemoteWithPath = path ->
                discoverer.discover(this.repository, new RemoteUrl(SSH, "example.com", -1, path), true)
                        .defaultIfEmpty(null)
                        .toBlocking().single();

        assertThat(discoverRepoForRemoteWithPath.apply("/short_path"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("/a//b"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("///a"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("//a"), nullValue());
        assertThat(discoverRepoForRemoteWithPath.apply("/"), nullValue());

        verifyZeroInteractions(registry);
        verifyZeroInteractions(probe);
    }
}
