package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.UriScheme;
import org.hamcrest.Matcher;
import org.junit.Test;

import static com.atlassian.bitbucket.linky.UriScheme.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class GitRemoteUrlParserTest {

    private GitRemoteUrlParser parser = new GitRemoteUrlParser();

    @Test
    public void testParseCloudHttpRemoteUrl() {
        assertUrlParsed("https://user@staging.bb-inf.net/bitbucket/test.git",
                is(HTTPS), is("staging.bb-inf.net"), is(-1), is("/bitbucket/test"));
        assertUrlParsed("http://user@bitbucket.org/atlas/some-project.git",
                is(HTTP), is("bitbucket.org"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("https://user@bitbucket.org/atlas/some-project.git",
                is(HTTPS), is("bitbucket.org"), is(-1), is("/atlas/some-project"));
    }

    @Test
    public void testParseCloudSshRemoteUrl() {
        assertUrlParsed("git-staging@bitbucket.org:atlas/some-project",
                is(SSH), is("bitbucket.org"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("git@bitbucket.org:atlas/some-project.git",
                is(SSH), is("bitbucket.org"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("ssh://git@bitbucket.org:atlas/some-project.git",
                is(SSH), is("bitbucket.org"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("ssh://git@bitbucket.org:atlas/some-project",
                is(SSH), is("bitbucket.org"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("git-staging@staging.bitbucket.org:atlas/some-project",
                is(SSH), is("staging.bitbucket.org"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("git@staging.bitbucket.org:atlas/some-project",
                is(SSH), is("staging.bitbucket.org"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("git-staging@staging.bb-inf.net:bitbucket/test.git",
                is(SSH), is("staging.bb-inf.net"), is(-1), is("/bitbucket/test"));
    }

    @Test
    public void testParseServerHttpRemoteUrl() {
        assertUrlParsed("http://user@some-host.com/atlas/some-project.git",
                is(HTTP), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("http://user@some-host.com/atlas/some-project",
                is(HTTP), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("http://git@some-host.com/some/path/atlas/some-project.git",
                is(HTTP), is("some-host.com"), is(-1), is("/some/path/atlas/some-project"));
        assertUrlParsed("http://git@some-host.com:7999/some/path/atlas/some-project.git",
                is(HTTP), is("some-host.com"), is(7999), is("/some/path/atlas/some-project"));
        assertUrlParsed("https://git@some-host.com/atlas/some-project.git",
                is(HTTPS), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("https://some-host.com/some/path/",
                is(HTTPS), is("some-host.com"), is(-1), is("/some/path"));
    }

    @Test
    public void testParseServerSshRemoteUrl() {
        assertUrlParsed("git://some-host.com/atlas/some-project.git",
                is(GIT), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("ssh://git@some-host.com/atlas/some-project.git",
                is(SSH), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("git@some-host.com/atlas/some-project.git",
                is(SSH), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("git@some-host.com/atlas/some-project.git",
                is(SSH), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("git@some-host.com/atlas/some-project.git",
                is(SSH), is("some-host.com"), is(-1), is("/atlas/some-project"));
        assertUrlParsed("ssh://git@some-host.com/some/path/atlas/some-project.git",
                is(SSH), is("some-host.com"), is(-1), is("/some/path/atlas/some-project"));
        assertUrlParsed("ssh://git@some-host.com:1234/some/path/atlas/some-project.git",
                is(SSH), is("some-host.com"), is(1234), is("/some/path/atlas/some-project"));
        assertUrlParsed("git@some-host.com:1234/some/path/atlas/some-project",
                is(SSH), is("some-host.com"), is(1234), is("/some/path/atlas/some-project"));
        assertUrlParsed("ssh://git@some-host.com:7999/some/path/~dpenkin/test-repository.git",
                is(SSH), is("some-host.com"), is(7999), is("/some/path/~dpenkin/test-repository"));
        assertUrlParsed("ssh://git@some-host.com:1234/some/path/atlas/some-project.git",
                is(SSH), is("some-host.com"), is(1234), is("/some/path/atlas/some-project"));
    }

    @Test
    public void testParseOtherRemoteUrls() {
        assertUrlParsed("example",
                is(SSH), is("example"), is(-1), is("/"));
        assertUrlParsed("http://example",
                is(HTTP), is("example"), is(-1), is("/"));
        assertUrlParsed("http://example.com",
                is(HTTP), is("example.com"), is(-1), is("/"));
        assertUrlParsed("http://example.com/",
                is(HTTP), is("example.com"), is(-1), is("/"));
        assertUrlParsed("http://example.com:239",
                is(HTTP), is("example.com"), is(239), is("/"));
        assertUrlParsed("http://example.com:239/",
                is(HTTP), is("example.com"), is(239), is("/"));
        assertUrlParsed("http://example.com:239/hello/world/",
                is(HTTP), is("example.com"), is(239), is("/hello/world"));
    }

    @Test
    public void testParseOtherSchemesReturnsNull() {
        assertThat(parser.parseRemoteUrl("file://whatever"), nullValue());
    }

    private void assertUrlParsed(String urlToParse,
                                 Matcher<UriScheme> schemeMatcher,
                                 Matcher<String> hostnameMatcher,
                                 Matcher<Integer> portMatcher,
                                 Matcher<String> pathMatcher) {
        RemoteUrl url = parser.parseRemoteUrl(urlToParse);
        assertThat(url, notNullValue());
        assertThat(url.getScheme(), schemeMatcher);
        assertThat(url.getHostname(), hostnameMatcher);
        assertThat(url.getPort(), portMatcher);
        assertThat(url.getPath(), pathMatcher);
    }
}
