//package com.atlassian.bitbucket.linky.repository;
//
//import com.atlassian.bitbucket.linky.blame.BlameLineReferenceProvider;
//import com.atlassian.bitbucket.linky.hosting.Hosting;
//import com.intellij.dvcs.repo.Repository;
//import com.intellij.openapi.vfs.VirtualFile;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import java.net.URI;
//import java.util.function.Supplier;
//
//import static com.atlassian.bitbucket.linky.repository.BitbucketRepository.VcsType.GIT;
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Mockito.when;
//
//@RunWith(MockitoJUnitRunner.class)
//public class CloudBitbucketRepositoryTest {
//    private final static String BRANCH_NAME = "branch#5><|.hello!&])({}@.@$&+_-`'\"";
//    private final static String ENCODED_BRANCH_NAME = "branch%235%3E%3C%7C.hello%21%26%5D%29%28%7B%7D%40.%40%24%26%2B_-%60%27%22";
//    private final static String TEST_URI = "http://example.com/";
//
//    @Mock
//    private BlameLineReferenceProvider blameLineReferenceProvider;
//    @Mock
//    private Hosting hosting;
//    @Mock
//    private VirtualFile rootDirectory;
//    @Mock
//    private Supplier<String> vcsReferenceSupplier;
//    @Mock
//    private Repository vcsRepository;
//
//    private CloudBitbucketRepository repository;
//
//    @Before
//    public void setUp() throws Exception {
//        when(vcsRepository.getCurrentBranchName()).thenReturn(BRANCH_NAME);
//
//        repository = CloudBitbucketRepository.builder(rootDirectory)
//                .bitbucket(hosting)
//                .vcsType(GIT)
//                .vcsRepository(vcsRepository)
//                .blameLineService(blameLineReferenceProvider)
//                .baseUri(URI.create(TEST_URI))
//                .vcsReferenceSupplier(vcsReferenceSupplier)
//                .build();
//    }
//
//    @Test
//    public void testGetPullRequestUri() {
//        URI pullRequestUri = repository.getPullRequestUri();
//
//        assertEquals("http://example.com/pull-requests/new?source=" + ENCODED_BRANCH_NAME, pullRequestUri.toString());
//    }
//}
