package com.atlassian.bitbucket;

import com.atlassian.bitbucket.linky.BitbucketRepository;
import com.atlassian.bitbucket.linky.CloudLinky;
import com.atlassian.bitbucket.linky.LinesSelection;
import com.atlassian.bitbucket.linky.blame.BlameLineReferenceProvider;
import com.atlassian.bitbucket.linky.hosting.BitbucketCloud;
import com.atlassian.bitbucket.linky.repository.BitbucketRepositorySettings;
import com.google.common.collect.ImmutableList;
import com.intellij.dvcs.repo.Repository;
import com.intellij.mock.MockVirtualFile;
import com.intellij.openapi.util.Pair;
import okhttp3.HttpUrl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class CloudLinkyTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private BitbucketRepositorySettings settings;
    @Mock
    private BlameLineReferenceProvider blameLineReferenceProvider;
    @Mock
    private Repository repository;

    private MockVirtualFile root;
    private MockVirtualFile file;
    private BitbucketRepository.Cloud cloud;
    private CloudLinky linky;

    @Before
    public void setUp() throws Exception {
        root = new MockVirtualFile("root");
        MockVirtualFile dir = new MockVirtualFile("dir");
        dir.setParent(root);
        file = new MockVirtualFile("file");
        file.setParent(dir);

        BitbucketCloud hosting = new BitbucketCloud(UUID.randomUUID(), HttpUrl.parse("http://example.com:1234/path").uri());
        cloud = new BitbucketRepository.Cloud(repository, hosting, "someOwner", "someSlug");

        linky = new CloudLinky(cloud, settings, blameLineReferenceProvider);

        when(blameLineReferenceProvider.getBlameLineReference(any(), eq(239))).thenReturn(Optional.empty());
        when(repository.getRoot()).thenReturn(root);
        when(repository.getCurrentRevision()).thenReturn("someRevision");
    }

    @Test
    public void testGetCommitViewUriWithLinesSelected() {
        when(blameLineReferenceProvider.getBlameLineReference(any(), eq(239))).thenReturn(
                Optional.of(Pair.create("some/File", 405)));

        URI commitUri = linky.getCommitViewUri("sha", file, 239);

        assertThat(commitUri, hasToString("http://example.com:1234/path/someOwner/someSlug/commits/sha#Lsome/FileT405"));
    }

    @Test
    public void testGetCommitViewUriWithBranchReference() {
        URI commitUri = linky.getCommitViewUri("branch-name", file, 239);

        assertThat(commitUri, hasToString("http://example.com:1234/path/someOwner/someSlug/commits/branch-name"));
    }

    @Test
    public void testGetCommitViewUriWithShaReference() {
        URI commitUri = linky.getCommitViewUri("abcdef", file, 239);

        assertThat(commitUri, hasToString("http://example.com:1234/path/someOwner/someSlug/commits/abcdef"));
    }

    @Test
    public void testGetSourceViewUri() {
        URI sourceUri = linky.getSourceViewUri();

        assertThat(sourceUri, hasToString("http://example.com:1234/path/someOwner/someSlug/src"));
    }

    @Test
    public void testGetSourceViewUriForFile() {
        URI sourceUri = linky.getSourceViewUri(file, Collections.emptyList());

        assertThat(sourceUri, hasToString("http://example.com:1234/path/someOwner/someSlug/src/someRevision/dir/file"));
    }

    @Test
    public void testGetSourceViewUriForFileWithSelections() {
        URI sourceUri = linky.getSourceViewUri(file, ImmutableList.of(LinesSelection.of(1, 7), LinesSelection.of(239, 405)));

        assertThat(sourceUri, hasToString("http://example.com:1234/path/someOwner/someSlug/src/someRevision/dir/file#file-1:7,239:405"));
    }

    @Test
    public void testGetPullRequestUriWhenBranchAbsent() {
        when(repository.getCurrentBranchName()).thenReturn(null);

        Optional<URI> maybePullRequestUri = linky.getPullRequestUri();

        assertThat(maybePullRequestUri.isPresent(), is(false));
    }

    @Test
    public void testGetPullRequestUriWhenBranchPresent() {
        when(repository.getCurrentBranchName()).thenReturn("someBranch");

        Optional<URI> maybePullRequestUri = linky.getPullRequestUri();

        assertThat(maybePullRequestUri.isPresent(), is(true));
        assertThat(maybePullRequestUri.get(), hasToString("http://example.com:1234/path/someOwner/someSlug/pull-requests/new?source=someBranch"));
    }
}
